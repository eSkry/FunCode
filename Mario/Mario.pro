TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

CONFIG(release, debug|release): LIBS += -lsfml-audio -lsfml-graphics -lsfml-network -lsfml-window -lsfml-system
CONFIG(debug, debug|release): LIBS += -lsfml-audio -lsfml-graphics -lsfml-network -lsfml-window -lsfml-system

CONFIG(release, debug|release): LIBS += -lpugixml
CONFIG(debug, debug|release): LIBS += -lpugixml

CONFIG(release, debug|release): LIBS += -lBox2D
CONFIG(debug, debug|release): LIBS += -lBox2D

SOURCES += main.cpp \
    system.cpp \
    level.cpp \
    levelmanager.cpp \
    camera.cpp \
    box2dworld.cpp \
    objects.cpp \
    datatilepropertys.cpp

HEADERS += \
    system.h \
    level.h \
    game.h \
    levelmanager.h \
    camera.h \
    box2dworld.h \
    objects.h \
    datatilepropertys.h
