
#include "GameMenu.h"

GameMenu::GameMenu(sf::RenderWindow &win) {
	pGame_window = &win;

	cursor = new Cursor("./content/skins/gui_cursor.png", sf::Vector2i(4, 4) ,sf::Vector2i(56, 56));
	cursor->getSprite().setOrigin(0, 0);
	cursor->getSprite().setScale(0.5, 0.5);

	textureBG.loadFromFile("./content/menu/bgMenu2.png");
	spriteBG.setTexture(textureBG);
	spriteBG.setPosition(-200, -60);

	SGTexture.loadFromFile("./content/menu/SGButton.png");
	SGButton.setTexture(SGTexture);
	SGButton.setPosition(0, 120);

	ITexture.loadFromFile("./content/menu/IButton.png");
	IButton.setTexture(ITexture);
	IButton.setPosition(0, 160);

	ETexture.loadFromFile("./content/menu/EButton.png");
	EButton.setTexture(ETexture);
	EButton.setPosition(0, 200);

}

void GameMenu::update() {
	
	sf::Vector2i pixelMousePos;
	sf::Vector2f mousePos;

	sf::Clock clock;

	while (pGame_window->isOpen()) {
		time = clock.getElapsedTime().asMicroseconds();
		time /= 800;
		clock.restart();

		pixelMousePos = sf::Mouse::getPosition(*pGame_window);
		moveBG(pixelMousePos, time);
		mousePos = pGame_window->mapPixelToCoords(pixelMousePos);

		cursor->update(mousePos);

		sf::Event event;
		while (pGame_window->pollEvent(event)) {

			if (event.type == sf::Event::Closed)
				pGame_window->close();

			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Escape))
				pGame_window->close();
			////////////////////
			if (EButton.getGlobalBounds().contains(pixelMousePos.x, pixelMousePos.y)) {
				if (event.key.code == sf::Mouse::isButtonPressed(sf::Mouse::Left)) 
					pGame_window->close();

				EButton.setColor(sf::Color::Cyan);
			}
			else {
				EButton.setColor(sf::Color::Green);
			}
			////////////////////
			if (SGButton.getGlobalBounds().contains(pixelMousePos.x, pixelMousePos.y)) {
				if (event.key.code == sf::Mouse::isButtonPressed(sf::Mouse::Left)) {
					return; 
				}
				SGButton.setColor(sf::Color::Cyan);
			}
			else {
				SGButton.setColor(sf::Color::Green);
			}
			////////////////////
			if (IButton.getGlobalBounds().contains(pixelMousePos.x, pixelMousePos.y)) {
				if (event.key.code == sf::Mouse::isButtonPressed(sf::Mouse::Left)) {
					//get info
				}
				IButton.setColor(sf::Color::Cyan);
			}
			else {
				IButton.setColor(sf::Color::Green);
			}
			////////////////////
		}
		////

		///
		drawMenu();
	}
}

void GameMenu::drawMenu() {

	pGame_window->clear();

	pGame_window->draw(spriteBG);
	pGame_window->draw(SGButton);
	pGame_window->draw(IButton);
	pGame_window->draw(EButton);
	pGame_window->draw(cursor->getSprite());

	pGame_window->display();
}

void GameMenu::moveBG(sf::Vector2i &mPos, float &time) {
	if (mPos.x < pGame_window->getSize().x / 2 - 100) {
		if (spriteBG.getPosition().x < 0)
			spriteBG.setPosition(spriteBG.getPosition().x + 0.05 * time, spriteBG.getPosition().y);
	}
	if (mPos.x > pGame_window->getSize().x / 2 + 100) {
		if (spriteBG.getPosition().x > -320)
			spriteBG.setPosition(spriteBG.getPosition().x + -0.05 * time, spriteBG.getPosition().y);
	}
	if (mPos.y < pGame_window->getSize().y / 2 - 50) {
		if (spriteBG.getPosition().y < 0)
			spriteBG.setPosition(spriteBG.getPosition().x, spriteBG.getPosition().y + 0.05 * time);
	}
	if (mPos.y > pGame_window->getSize().y / 2 + 50) {
		if (spriteBG.getPosition().y > -180)
		spriteBG.setPosition(spriteBG.getPosition().x, spriteBG.getPosition().y + -0.05 * time);
	}
}

GameMenu::~GameMenu() {
	delete cursor;
}