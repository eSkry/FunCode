#ifndef GAME_H
#define GAME_H

class Game{
public:
    Game() {}
    ~Game() {}

    virtual void mainFunc() = 0;

    virtual void eventFunc() = 0;
};

#endif // GAME_H
