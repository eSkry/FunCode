#include "PixelMatrix.h"





template<int width_t, int height_t>
void PixelMatrix<width_t, height_t>::saveToFile(std::string fileName) {
	std::ofstream of(fileName);

	for (int i = 0; i < height_t; i++) {
		for (int j = 0; j < width_t; j++) {
			of << '[' << (int)matrix[i][j].r << ',' << (int)matrix[i][j].g << ',' << (int)matrix[i][j].b << ',' << (int)matrix[i][j].a << ']';
		}
		of << '\n';
	}

	of.close();
}
