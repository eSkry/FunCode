package eskry.newworld.blocks;

import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.block.material.Material;

public class RegisterBlocks {

    public static ManganeseOreBlock manganeseOreBlock = new ManganeseOreBlock("ManganeseOreBlock", Material.rock);
    public static ParquetBock parquetBockBrick = new ParquetBock("ParquetBrick", "parquet_brick", Material.wood);

    public static void init(){

        GameRegistry.registerBlock(manganeseOreBlock, "ManganeseOreBlock");
        GameRegistry.registerBlock(parquetBockBrick, "ParquetBrick");

    }

}
