#include "genfilename.h"

GenFileName::GenFileName(){

}

std::string GenFileName::getRandFileName(unsigned short int length, std::string directory, std::string file_type, bool upReg, bool downReg, bool numbers){
    char cupReg[] = "QWERTYUIOPASDFGHJKLZXCVBNM";
    char cdownReg[] = "qwertyuiopasdfghjklzxcvbnm";
    char cnumbers[] = "0123456789";

    if (directory[ directory.length() ] != '/')
        directory += '/';

    std::string fileName;
    fileName += directory;

    int temp = 0;

    if (!upReg && !downReg && !numbers)
        return std::string("error parameters of genRandFileName"); // Нечего генерировать

    for (int i = 0; i < length; i++){
        while (true){
            temp = rand()%3;
            if (temp == 0 && upReg){
                fileName += cupReg[rand()%26];
                break;
            }
            if (temp == 1 && downReg){
                fileName += cdownReg[rand()%26];
                break;
            }
            if (temp == 2 && numbers){
                fileName += cnumbers[rand()%10];
                break;
            }
        }
    }

    fileName += '.';
    fileName += file_type;
    return fileName;
}
