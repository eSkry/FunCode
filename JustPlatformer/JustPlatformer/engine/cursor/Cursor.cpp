
#include "Cursor.h"


sf::Sprite& Cursor::getSprite() {
	return sprite;
}

void Cursor::update(sf::Vector2f posMouse) {

	sprite.setPosition(posMouse);
}

Cursor::Cursor(sf::String fileName, sf::Vector2i leftTop, sf::Vector2i size) {

	texture.loadFromFile(fileName);
	texture.setSmooth(true);
	sprite.setTexture(texture);

	sizeCursor.x = size.x;
	sizeCursor.y = size.y;

	sprite.setOrigin(size.x / 2, size.y / 2);

	sprite.setTextureRect(sf::IntRect(leftTop.x, leftTop.y, size.x, size.y));

}