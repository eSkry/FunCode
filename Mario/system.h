#ifndef SYSTEM_H
#define SYSTEM_H

/////////// STL ///////////
#include <iostream>
#include <cstring>
#include <string>
#include <vector>
#include <list>
#include <map>
using namespace std;

/////////// PUGI ///////////
#include <pugixml.hpp>

/////////// SFML ///////////
#include <SFML/Graphics.hpp>
using namespace sf;

/////////// MARIO ///////////
#include "game.h"

struct Mario{
    Mario();

    int                 iHealth;
    int                 iLevel;
    int                 iScore;
    int                 iMarioType;
};

struct Configuration{
    Configuration();

    int                 iWindowWidth;
    int                 iWindowHeight;
    int                 iFramerateLimit;
    int                 iGameSpeed;

    float               fTick;

    bool                bV_Sinc;
    bool                bCursorVisible;

    Uint32              uStyle;

    string              sWindowName;
};

class System{
public:
    System(Configuration config);
    ~System();

    // Главный игровой цикл
    void mainLoop(Game *game);

    Vector2f& getCursorPosGame();
    Vector2i& getCursorPosWindow();

    float getTime();

    Event& getEvent();

    RenderWindow* getRenderWindow();

private:
    // MARIO //
    Mario               mMarioStats;

    // SYSTEM //
    Configuration       cConfiguration;

    RenderWindow*       rRenderWindow;

    float               fTime;

    Clock               cClock;

    Event               eEvent;

    Vector2i            vCursorPositionWindow;
    Vector2f            vCursorPositionGame;

    Color               cColorWindow;

    // Создание окна
    void CreateWindow();
    // Обновление времени
    void ClockUpdate();
    // Обновление позиции курсора
    void CursorUpdate();
};



#endif // SYSTEM_H
