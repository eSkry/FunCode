#include "box2dworld.h"

Box2DWorld::Box2DWorld(b2AABB aabb, b2Vec2 gravity, bool doSleep){
    this->doSleep               = doSleep;
    bGravity                    = gravity;
    bWorldAABB                  = new b2AABB(aabb);
    bWorld                      = new b2World(gravity);
}

Box2DWorld::~Box2DWorld(){
    delete bWorldAABB;
    delete bWorld;
}

void Box2DWorld::createGroundBox(FloatRect objRect){
    b2PolygonShape shape;
    shape.SetAsBox( (objRect.width / 2.f) / SCALE, (objRect.height / 2.f) / SCALE);

    b2BodyDef bDef;
    bDef.position.Set( (objRect.left + (objRect.width / 2)) / SCALE, (objRect.top + (objRect.height / 2)) / SCALE );
    //bDef.position.Set( objRect.left / SCALE, objRect.top / SCALE );
    bDef.type = b2_staticBody;

    b2Body *bd = bWorld->CreateBody(&bDef);
    bd->CreateFixture(&shape, 1);

    lBodysBoxStatic.push_back(bd);
}

void Box2DWorld::createDynamicBox(FloatRect objRect, bool fixedRotation){
    lBodysBoxDynamic.push_back(createPlayer(objRect, fixedRotation));
}

b2Body* Box2DWorld::createPlayer(FloatRect objRect, bool fixedRotation){
    b2PolygonShape shape;
    shape.SetAsBox( (objRect.width / 2.f) / SCALE, (objRect.height / 2.f) / SCALE);

    b2BodyDef bDef;
    //bDef.position.Set( (objRect.left + (objRect.width / 2)) / SCALE, (objRect.top + (objRect.height / 2)) / SCALE );
    bDef.position.Set( objRect.left / SCALE, objRect.top / SCALE );
    bDef.type = b2_dynamicBody;
    bDef.fixedRotation = fixedRotation;

    bPlayer = bWorld->CreateBody(&bDef);
    bPlayer->CreateFixture(&shape, 1);

    return bPlayer;
}

b2World* Box2DWorld::getWorld(){
    return bWorld;
}
