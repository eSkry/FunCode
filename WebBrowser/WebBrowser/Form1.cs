﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WebBrowser {
    public partial class Form1 : Form {
        public Form1() {
            InitializeComponent();
            webBrowser1.Navigate("http://google.com");
        }

        private void btBack_Click(object sender, EventArgs e) {
            webBrowser1.GoBack();
        }

        private void btForward_Click(object sender, EventArgs e) {
            webBrowser1.GoForward();
        }

        private void btRefresh_Click(object sender, EventArgs e) {
            webBrowser1.Refresh();
        }

        private void btGo_Click(object sender, EventArgs e) {
            if (!string.IsNullOrEmpty(textBox1.Text)) {
                webBrowser1.Navigate(textBox1.Text);
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e) {

        }

        private void textUrl_KeyPress(object sender, KeyPressEventArgs e) {
            if (e.KeyChar == (char)13) {
                webBrowser1.Navigate(textBox1.Text);
            }
        }
    }
}
