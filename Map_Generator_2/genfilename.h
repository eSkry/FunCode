#ifndef GENFILENAME_H
#define GENFILENAME_H

#include <string>

class GenFileName{
public:
    GenFileName();

    static std::string getRandFileName(unsigned short int length, std::string directory, std::string file_type, bool upReg = true, bool downReg = true, bool numbers = true);
};

#endif // GENFILENAME_H
