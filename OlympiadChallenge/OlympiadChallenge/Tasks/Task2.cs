﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

///     Задача №2 (1.1.2 в книге)
/// Решить предыдущую задачу, не используя дополнительных переменных
/// (и предполагая, что значениями целых переменных могут быть произвольные целые числа).
/// 

namespace OlympiadChallenge.Tasks {
    class Task2 {

        public static void Run(int a, int b) {
            Task t = new Task();
            t.ShowTask();
            Console.WriteLine("Стартовые значения - a={0}, b={1}", a, b);

            a = a + b;
            b = a - b;
            a = a - b;

            Console.WriteLine("Выходные значения - a={0}, b={1}\n", a, b);
        }

    }
}
