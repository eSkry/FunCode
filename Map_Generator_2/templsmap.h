#ifndef TEMPLSMAP_H
#define TEMPLSMAP_H

#include "pugixml.hpp"
#include <iostream>
#include <string>
#include <string.h>
#include <cstdlib>
#include <vector>

class templsMap{
public:
    using TM_SI = short int;
    using TM_USI = unsigned short int;

    TM_SI** getRandomTemplate();

    templsMap();
    ~templsMap();

    void addTemplatesOfFile(std::string fileName);


    TM_USI m_Width, m_Height, m_CountObjects;

    std::vector<TM_SI**> m_Data;

};

#endif // TEMPLSMAP_H
