﻿using System;
using System.Threading;

namespace SnakeApp {
    class Player {

        private char headSkin = 'O';
        private char bodySkin = '*';
        private Vector2i pos;

        Dir currentDir = Dir.RIGHT;

        public enum Dir {
            UP = 0,
            DOWN,
            LEFT,
            RIGHT
        }

        public Dir getDir() {
            return currentDir;
        }

        public void move(int x, int y) {
            pos.x += x;
            pos.y += y;
        }

        public void move(Vector2i move) {
            pos.x += move.x;
            pos.y += move.y;
        }

        public Player(int x, int y) {
            pos = new Vector2i(x, y);
        }

        public Vector2i getPosition() {
            return pos;
        }

        public void update() {
            сontrols();

            switch (currentDir) {
                case Dir.UP:    --pos.y;   break;
                case Dir.DOWN:  ++pos.y;   break;
                case Dir.LEFT:  --pos.x;   break;
                case Dir.RIGHT: ++pos.x;   break;
            }
        }

        public void print() {
            Console.SetCursorPosition(pos.x, pos.y);
            Console.ForegroundColor = System.ConsoleColor.Red;
            Console.Write(headSkin);

            Console.ResetColor();
        }

        private void сontrols() {
            ConsoleKey key = Console.ReadKey().Key;

            switch (key) {
                case ConsoleKey.A: currentDir = Dir.LEFT;   break;
                case ConsoleKey.W: currentDir = Dir.UP;     break;
                case ConsoleKey.S: currentDir = Dir.DOWN;   break;
                case ConsoleKey.D: currentDir = Dir.RIGHT;  break;
            }

        }

    }
}
