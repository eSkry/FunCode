package eskry.newworld.blocks;

import eskry.newworld.utils.ModInfo;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;

public class ParquetBock extends Block {

    public ParquetBock(String name, String textureName, Material material){
        super(material);
        setBlockName(name);
        setBlockTextureName(ModInfo.modid + ':' + textureName);
        setCreativeTab(CreativeTabs.tabBlock);
        setHardness(2.0F);
        setResistance(6.0F);
        setLightLevel(0.0F);
        setHarvestLevel("axe", 1);
        setStepSound(soundTypeWood);
    }

}
