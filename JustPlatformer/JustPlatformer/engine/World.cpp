#include "World.h"

bool World::loadMap(char *fileName) {
	pugi::xml_document doc;

	if (!doc.load_file(fileName))
		return false;

	gravitation = new b2Vec2(0.f, 180.f);
	world = new b2World(*gravitation);

	pugi::xml_node map = doc.child("map");
	pugi::xml_node tileset = map.child("tileset");

	try {
		if (map == NULL)
			throw "Map error, not find 'map' tag.";

		if (tileset == NULL)
			throw "Map error, not find 'tileset' tag.";
	}
	catch (char *error) {
		errorReporter(error);
		loaded = NOT_LOADED;
		return false;
	}


	widthMap = map.attribute("width").as_int();
	heightMap = map.attribute("height").as_int();

	// ��������: ����� ������
	// ������ ������� ���������
	// ���-�� ���������� �����
	while (tileset) {

		Tileset TempTiles;
		TempTiles.firstgid = tileset.attribute("firstgid").as_int();
		TempTiles.tilecount = tileset.attribute("tilecount").as_int();
		TempTiles.tileWidth = tileset.attribute("tilewidth").as_int();
		TempTiles.tileHeight = tileset.attribute("tileheight").as_int();
		string TempDir = tileset.child("image").attribute("source").as_string();;
		TempTiles.texture.loadFromFile(TempDir);
		TempTiles.texture.setSmooth(true);

		countTilesetLayers++;
		tileCountInWorld += TempTiles.tilecount;

		// ��������� �������� �� ������ � ������������
		// ������� ����� ���� ID ������� � ��� firstgid
		// �� tilecount
		int imgTileCountX = TempTiles.texture.getSize().x / TempTiles.tileWidth;
		int imgTileCountY = TempTiles.texture.getSize().y / TempTiles.tileHeight;
		int countTileID = TempTiles.firstgid;
		tilesets.push_back(TempTiles);

		for (int i = 0; i < imgTileCountY; i++) {
			for (int j = 0; j < imgTileCountX; j++) {

				sf::Sprite TempSpr;

				TempSpr.setTextureRect(sf::IntRect(j * TempTiles.tileWidth, i * TempTiles.tileHeight, TempTiles.tileWidth, TempTiles.tileHeight));
				//TempSpr.setScale(0.5, 0.5);

				sprites.insert(std::pair<int, sf::Sprite>(countTileID, TempSpr));
				countTileID++;
			}
		}


		tileset = tileset.next_sibling("tileset");
	}
	setTextureTiles();

	// �������� ����� ������
	pugi::xml_node layer = doc.child("map").child("layer");
	while (layer) {

		Layer TempLayer;
		TempLayer.width = layer.attribute("width").as_int();
		TempLayer.height = layer.attribute("height").as_int();

		pugi::xml_node data = layer.child("data");
		string TempLayerString = data.text().as_string();
		string tempStrNum;

		// �������������� ����� �� ������ � ���
		// ��������� ������ ����� ���-�� ����� ������������
		for (char i : TempLayerString) {
			if (i != ',') {
				tempStrNum += i;
			} else {
				TempLayer.layer.push_back(atoi(tempStrNum.c_str()));
				tempStrNum.clear();
			}
		}
		TempLayer.layer.push_back(atoi(tempStrNum.c_str()));

		layers.push_back(TempLayer);

		layer = layer.next_sibling("layer");
	}


	// �������� �������� �� �����
	// ��� �������� �� ���������� 
	// ���������� �� ����
	b2BodyDef *tmpBodyDef = new b2BodyDef;
	b2PolygonShape *tmpPlgShape = new b2PolygonShape;
	b2FixtureDef *tmpFixtDef = new b2FixtureDef;

	pugi::xml_node objectgroup = map.child("objectgroup");
	while (objectgroup) {

		pugi::xml_node object = objectgroup.child("object");
		while (object) {
			Object TempObj;

			if (object.attribute("name") != NULL)
				TempObj.name = object.attribute("name").as_string();

			if (object.attribute("type") != NULL)
				TempObj.type = object.attribute("type").as_string();

			tmpBodyDef->type = b2_staticBody;

			if (strcmp(object.attribute("type").as_string(), "player") == 0) {
				tmpBodyDef->type = b2_dynamicBody;
				tmpFixtDef->friction = 0.65f;
				tmpFixtDef->density = 0.5f;
			}
			else {
				tmpFixtDef->friction = 0.2f;
				tmpFixtDef->density = 1;
			}

			tmpBodyDef->position.Set((object.attribute("x").as_int() + (object.attribute("width").as_int() / 2)) / GampeConst::SCALE, (object.attribute("y").as_int() + (object.attribute("height").as_int() / 2)) / GampeConst::SCALE);
			tmpPlgShape->SetAsBox((object.attribute("width").as_int() / 2) / GampeConst::SCALE, (object.attribute("height").as_int() / 2) / GampeConst::SCALE);
			tmpFixtDef->shape = tmpPlgShape;
			TempObj.body = world->CreateBody(tmpBodyDef);
			//TempObj.body->CreateFixture(tmpPlgShape, 1);
			TempObj.body->CreateFixture(tmpFixtDef);


			if (tmpBodyDef->type == b2_staticBody) {
				staticObjects.push_back(TempObj);
			} else {
				dynamicObjects.push_back(TempObj);
			}


			object = object.next_sibling("object");
		}

		objectgroup = objectgroup.next_sibling("objectgroup");
	}


	// Delete
	delete tmpPlgShape;
	delete tmpBodyDef;
	delete tmpFixtDef;

	return true;
}

void World::setTextureTiles() {
	int count = 1;
	int tilecount;
	for (auto tileList = tilesets.begin(); tileList != tilesets.end(); ++tileList) {
		tilecount = tileList->tilecount - 1;
		for (int i = 0; i < tilecount; i++) {
			sprites.find(count)->second.setTexture(tileList->texture);
			++count;
		}
	}

}

void World::drawMap(sf::RenderWindow &game) {

	x = y = 0;
	for (auto layerTiles = layers.begin(); layerTiles != layers.end(); ++layerTiles) {
		x = y = 0;
		for (auto tileID = layerTiles->layer.begin(); tileID != layerTiles->layer.end(); tileID++) {

			if (x == layerTiles->width) {
				x = 0;
				y++;
			}

			if (*tileID == 0) {
				x++;
				continue;
			}
			
			sprites.find(*tileID)->second.setPosition(x * 64, y * 64);
				

			game.draw(sprites.find(*tileID)->second);
			x++;
		}

	}
	
}

void World::errorReporter(char *error) const {
	std::cout << error << std::endl;
}

void World::update() {
	world->Step(1 / 60.f, 4, 2);
}

b2Body* World::getPlayerObject() {
	
	for (auto it = dynamicObjects.begin(); it != dynamicObjects.end(); it++) {
		if (strcmp(it->type.c_str(), "player") == 0) {
			playerObject = it->body;
		}
	}
	playerObject->SetFixedRotation(true);
	return playerObject;
}

World::World() {
	tileCountInWorld = 0;
	widthMap = 0;
	heightMap = 0;
	countTilesetLayers = 0;
	x = 0;
	y = 0;
}


World::~World(){
	delete gravitation;
	delete world;
}

