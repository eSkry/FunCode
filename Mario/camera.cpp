#include "camera.h"

Camera::Camera(FloatRect rect, IntRect bounds){
    fRect = rect;
    iBounds = bounds;
    vView = new View(fRect);
}

Camera::~Camera(){
    delete vView;
}

View* Camera::getView(){
    return vView;
}

FloatRect& Camera::getRect(){
    return fRect;
}

void Camera::update(Vector2f pos){

    if (pos.x < iBounds.left){
        pos.x = iBounds.left;
    }
    if (pos.x > iBounds.width){
        pos.x = iBounds.width;
    }
    /*if (pos.y > iBounds.top){
        pos.y = iBounds.top;
    }*/
    if (pos.y < iBounds.height){
        pos.y = iBounds.height / 2 + 10;
    }

    vView->setCenter(pos);
}
