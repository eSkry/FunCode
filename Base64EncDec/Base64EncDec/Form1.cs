﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Base64EncDec {
    public partial class Form1 : Form {
        public Form1() {
            InitializeComponent();
        }

        private void btEncode_Click(object sender, EventArgs e) {
            byte[] normalText = Encoding.ASCII.GetBytes(tbNormalText.Text);
            string encodetText = Convert.ToBase64String(normalText);
            tbBase64Text.Text = encodetText;
        }

        private void btDecode_Click(object sender, EventArgs e) {
            byte[] normalText = Convert.FromBase64String(tbBase64Text.Text);
            string decodeText = Encoding.ASCII.GetString(normalText);
            tbNormalText.Text = decodeText;
        }
    }
}
