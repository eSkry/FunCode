﻿namespace Base64EncDec {
    partial class Form1 {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent() {
            this.btEncode = new System.Windows.Forms.Button();
            this.btDecode = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.tbNormalText = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.tbBase64Text = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // btEncode
            // 
            this.btEncode.Location = new System.Drawing.Point(13, 13);
            this.btEncode.Name = "btEncode";
            this.btEncode.Size = new System.Drawing.Size(142, 39);
            this.btEncode.TabIndex = 0;
            this.btEncode.Text = "Encode";
            this.btEncode.UseVisualStyleBackColor = true;
            this.btEncode.Click += new System.EventHandler(this.btEncode_Click);
            // 
            // btDecode
            // 
            this.btDecode.Location = new System.Drawing.Point(161, 13);
            this.btDecode.Name = "btDecode";
            this.btDecode.Size = new System.Drawing.Size(142, 39);
            this.btDecode.TabIndex = 1;
            this.btDecode.Text = "Decode";
            this.btDecode.UseVisualStyleBackColor = true;
            this.btDecode.Click += new System.EventHandler(this.btDecode_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(10, 59);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(64, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Normal Text";
            // 
            // tbNormalText
            // 
            this.tbNormalText.Location = new System.Drawing.Point(13, 75);
            this.tbNormalText.Multiline = true;
            this.tbNormalText.Name = "tbNormalText";
            this.tbNormalText.Size = new System.Drawing.Size(290, 68);
            this.tbNormalText.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 150);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(67, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Base64 Text";
            // 
            // tbBase64Text
            // 
            this.tbBase64Text.Location = new System.Drawing.Point(12, 166);
            this.tbBase64Text.Multiline = true;
            this.tbBase64Text.Name = "tbBase64Text";
            this.tbBase64Text.Size = new System.Drawing.Size(291, 71);
            this.tbBase64Text.TabIndex = 5;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(317, 247);
            this.Controls.Add(this.tbBase64Text);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.tbNormalText);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btDecode);
            this.Controls.Add(this.btEncode);
            this.MaximumSize = new System.Drawing.Size(333, 286);
            this.MinimumSize = new System.Drawing.Size(333, 286);
            this.Name = "Form1";
            this.Text = "Base64 Encoder Decoder";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btEncode;
        private System.Windows.Forms.Button btDecode;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbNormalText;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tbBase64Text;
    }
}

