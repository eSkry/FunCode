﻿#include <SFML/Graphics.hpp>

#include "engine/GameMenu.h"
#include "engine/World.h"
#include "engine/Camera.h"
#include "engine/Player.h"
#include "engine/cursor/Cursor.h"

using namespace sf;

int main(int *argc, char **argv) {

	sf::ContextSettings settings;
	settings.antialiasingLevel = 2;

	RenderWindow game_window(VideoMode(1280, 780), "JustPlatformer v0.2 Alpha", sf::Style::Default, settings);
	game_window.setFramerateLimit(70);
	game_window.setActive(true);
	game_window.setMouseCursorVisible(false);
	game_window.setVerticalSyncEnabled(false);

	GameMenu *GlobalMenu = new GameMenu(game_window);
	GlobalMenu->update();
	delete GlobalMenu;

	//View
	Camera camera(FloatRect(0, 0, 1280, 780));
	
	//World
	World *level = new World;
	level->loadMap("./content/maps/level1.tmx");

	//Player
	Player player("./content/skins/player.png", 82, 75);
	player.getSprite().setTextureRect(IntRect(7, 4, player.getWidth(), player.getWidth()));
	level->getPlayerObject();
	player.setObjectPlayer(level->playerObject);

	//Cursor
	Cursor cursor("././content/skins/Hellgame.png", sf::Vector2i(4, 2) ,sf::Vector2i(57, 59));
	sf::Vector2i pixelPos;
	sf::Vector2f mousePos;
	
	//Time
	Clock clock;

	while (game_window.isOpen()) {
		float time = clock.getElapsedTime().asMicroseconds();
		time /= 800;
		clock.restart();

		//Cursor position
		pixelPos = Mouse::getPosition(game_window);
		mousePos = game_window.mapPixelToCoords(pixelPos);

		Event event;
		while (game_window.pollEvent(event)) {

			if (event.type == Event::Closed)
				game_window.close();
			if (Keyboard::isKeyPressed(Keyboard::Escape))
				game_window.close();

		}

		/////// UPDATES ///////
		cursor.update(mousePos);

		player.update(time);
		level->update();
		camera.update(player.getCoords().x, player.getCoords().y);
		//camera.moovingCamera(time);
		/////// END UPDATES ///////

		game_window.clear(sf::Color::Cyan);
		level->drawMap(game_window);

		game_window.draw(player.getSprite());
		game_window.draw(cursor.getSprite());

		game_window.setView(camera.getView());
		game_window.display();
	}
	
}