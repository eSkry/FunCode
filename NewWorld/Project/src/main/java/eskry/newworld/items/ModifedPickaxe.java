package eskry.newworld.items;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.ItemPickaxe;

public class ModifedPickaxe extends ItemPickaxe {

    public ModifedPickaxe(String name, ToolMaterial toolMaterial, String textureName){
        super(toolMaterial);
        setUnlocalizedName(name);
        setTextureName("newworld:" + textureName);
        setCreativeTab(CreativeTabs.tabTools);
    }

}
