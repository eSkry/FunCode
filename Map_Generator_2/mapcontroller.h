#ifndef MAPCONTROLLER_H
#define MAPCONTROLLER_H

#include <string>

#include "makeworldtemplate.h"
#include "genmapway.h"

using std::string;

class MapController{
public:
    using MC_USI = unsigned short int;
    using MC_SI = short int;

    MapController();

    enum GenSettings {
        Default,
        Other
    };

    GenSettings settings;

    void setSaveInFile(bool save = true); // если true то сохранение мира возможно иначе мир не сможет сохраниться в фаил
    void resetSettings(); // сбрасывает все параметры в параметры по умолчанию
    //

    // for map way metods
    void MapWay_setCycles(MC_USI cycles);
    void MapWay_setSaveFileName(string fileName); // Устанавливает имя для сохранение файла
    void MapWay_setWorldSizes(MC_USI width, MC_USI height); // устанавливает размер для генерации карты путей
    void MapWay_loadFromFile(string fileName); // Загружает карту из файла
    void MapWay_saveInFile(); // Сохраняет карту в фаил
    void MapWay_startGeneration(); // Запускает генерацию
    genMapWay::GMW_SI** MapWay_getMapWay(); // дает ссылку на мир

    // for map gen methods
    void Map_setSaveFileName(string fileName);
    void Map_loadFromFile(string fileName);
    void Map_saveInFile();
    void Map_setTemplateSizes(string size);
    void Map_startGeneration();
    MC_USI Map_getWidth() const;
    MC_USI Map_getHeihgt() const;
    MakeWorldTemplate::MWT_SI** Map_getMap();

private:
    // for map way generator
    genMapWay m_MW_genMapWay;
    MC_USI m_MW_width, m_MW_height, m_MW_cycles;
    bool m_MW_saveRandFile, m_MW_isLoadedMap;
    string m_MW_SaveFileName, m_MW_LoadFileName;

    // for map generator
    MakeWorldTemplate m_MWT_makeWorldTemplate;
    bool m_MWT_saveRandFile, m_MWT_isLoadedMap;
    string m_MWT_SaveFileName, m_MWT_LoadFileName;
    string m_MWT_templateType;

    // OTHER
    bool m_saveInFile;

};

#endif // MAPCONTROLLER_H
