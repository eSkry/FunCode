
#include "system.h"
#include "camera.h"
#include "level.h"
#include "levelmanager.h"

#include "box2dworld.h"

class MyGame : public Game{
public:
    MyGame(System* system){
        this->system = system;

        //////////////// LOAD LEVEL ////////////////
        levelManager = new LevelManager(system);
        levelManager->addMapDirectory( "content/maps/level1_1.tmx" );

        levelManager->selectLevel(0);
        //////////////// LOAD LEVEL ////////////////

        //////////////// BOX2D WORLD ////////////////
        b2AABB tempAABB;
        tempAABB.lowerBound.Set(-1000, -1000);
        tempAABB.upperBound.Set(1000, 1000);
        b2Vec2 gravity(0, 8);
        bWorld = new Box2DWorld(tempAABB, gravity, false);

        const int objectsCount = 7;
        string objectNames[objectsCount] = {"platform","brick","killBlock","coinbox","block","trumpet","trumpetPortal"};

        for (int i = 0; i < objectsCount; i++){
            LRect objects = levelManager->getLevel()->getRectsWithType(objectNames[i]);
            for (auto it = objects.begin(); it != objects.end(); it++){
                bWorld->createGroundBox( (*it)->fRect );
            }
        }

        // PLAYER
        LRect pl = levelManager->getLevel()->getRectsWithType("player");
        levelPlayer = (*pl.begin());
        bPlayer = bWorld->createPlayer(levelPlayer->fRect, true);

        levelPlayer->tTexture = new Texture;
        levelPlayer->sSprite = new Sprite;

        levelPlayer->tTexture->loadFromFile("content/data/marioSkin.png");
        levelPlayer->sSprite->setTexture( *levelPlayer->tTexture );

        IntRect tempPlRect;
        tempPlRect.left = 0;
        tempPlRect.top = 0;
        tempPlRect.height = levelPlayer->fRect.height;
        tempPlRect.width = levelPlayer->fRect.width;

        levelPlayer->sSprite->setTextureRect(tempPlRect); // TODO: Это только для теста прямоугольнк нужно вырезать из тайла анимации и реализовывать в классе анимации.
        levelPlayer->sSprite->setOrigin(tempPlRect.width / 2, tempPlRect.height / 2);
        //////////////// BOX2D WORLD ////////////////

        ////////////////   CAMERA   ////////////////
        cameraSpeed = 0.5;
        FloatRect rect;
        Vector2u size = system->getRenderWindow()->getSize();
        rect.left = 0;
        rect.top = 0;
        rect.width = size.x;
        rect.height = size.y;
        camera = new Camera(rect, IntRect(0, 0, levelManager->getLevel()->getWorldWidthPixel(), levelManager->getLevel()->getWorldHeightPixel()));
        camera->getView()->zoom(0.3);
        camera->getView()->setCenter(168, 130);
        ////////////////   CAMERA   ////////////////

    }

    void playerUpdate();

    void mainFunc(){
        control();
        playerUpdate();

        bWorld->getWorld()->Step(1/60.f, 7, 3);

        system->getRenderWindow()->setView( *camera->getView() );
        //// DRAW ////

        /// SECTOR MAP DRAW ///
        levelManager->drawLevel();
        /// SECTOR MAP DRAW ///

        system->getRenderWindow()->draw( *levelPlayer->sSprite );

        //// DRAW ////
    }

    void eventFunc(){
        // Функция внутри цикла обработчика событий


    }

    void control(){
        if (Keyboard::isKeyPressed(Keyboard::Escape)){
            system->getRenderWindow()->close();
        }
        if (Keyboard::isKeyPressed(Keyboard::Space)){
            bPlayer->ApplyForceToCenter(b2Vec2(0, -4), 1);
        }
        if (Keyboard::isKeyPressed(Keyboard::A)){
            bPlayer->ApplyForceToCenter(b2Vec2(-3, 0), 1);

        }
        if (Keyboard::isKeyPressed(Keyboard::D)){
            bPlayer->ApplyForceToCenter(b2Vec2(3, 0), 1);
        }
    }

    ~MyGame(){
        delete camera;
        delete levelManager;
        delete bWorld;
    }

private:
    float cameraSpeed;
    Camera*             camera;

    LevelManager*       levelManager;
    Box2DWorld*         bWorld;

    Rect_ML*            levelPlayer;
    b2Body*             bPlayer;
    // NOT DELETE THIS CLASS
    System* system;
};

void MyGame::playerUpdate(){
    b2Vec2 temp = bPlayer->GetPosition();
    float angle = bPlayer->GetAngle();

    levelPlayer->sSprite->setPosition((temp.x * SCALE), (temp.y * SCALE));
    levelPlayer->sSprite->setRotation(angle * DEG);

    camera->update( levelPlayer->sSprite->getPosition() );
}

int main(){
    Configuration startConfig;
    startConfig.iWindowWidth        = 1100;
    startConfig.iWindowHeight       = 700;
    startConfig.sWindowName         = "Mario";
    startConfig.bV_Sinc             = true;
    startConfig.iFramerateLimit     = 60;

    System system(startConfig);
    MyGame game(&system);


    system.mainLoop(&game);
    return 0;
}
