﻿using System;
using System.Threading;

namespace MyHttpServer {
    class Program {
        static void Main(string[] args) {

            HttpServer httpServer = new MyHttpServer(7777);
            Thread thread = new Thread(new ThreadStart(httpServer.listen));
            thread.Start();

        }
    }
}
