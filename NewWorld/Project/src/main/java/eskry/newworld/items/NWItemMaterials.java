package eskry.newworld.items;

import net.minecraft.item.Item;
import net.minecraftforge.common.util.EnumHelper;

public class NWItemMaterials {

    public static Item.ToolMaterial ToolMatIronPickaxeM1
            = EnumHelper.addToolMaterial("newworld:ironpickaxem1",
            2, 380, 15.0F, 2.0F, 15);

    public static Item.ToolMaterial ToolMatIronPickaxeM2
            = EnumHelper.addToolMaterial("newworld:ironpickaxem2",
            2, 500, 18.0F, 2.5F, 15);

    public static Item.ToolMaterial ToolMatIronPickaxeM3
            = EnumHelper.addToolMaterial("newworld:ironpickaxem3",
            2,850, 20.0F, 3.0F, 15);

    public static Item.ToolMaterial ToolMaterialExtremalPixkaxe
            = EnumHelper.addToolMaterial("newworld:extremalpickaxe",
            4, 3000, 50.F, 10, 15);

}
