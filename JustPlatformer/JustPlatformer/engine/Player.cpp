#include "Player.h"


Player::Player(){
	objTexture.loadFromFile("./content/skins/player.png");
	objSprite.setTexture(objTexture);
	objSprite.setTextureRect(sf::IntRect(8, 3, 82, 75));
	objSprite.setPosition(0, 0);
	jumpHeight = 0;
	isJump = onGround = false;
	/*LeftButton_ = new MoveLeft;
	RightButton_ = new MoveRight;
	JumpButton_ = new MoveJump;*/
}

void Player::update(float &time) {
	if (isLife) {
		moovingPlayer();
	}

	objSprite.setPosition((playerObj->GetPosition().x * GampeConst::SCALE) - 5, (playerObj->GetPosition().y * GampeConst::SCALE) - 10);

	//objSprite.setRotation(playerObj->GetAngle() * GampeConst::DEG);

	pos = objSprite.getPosition();
}

void Player::moovingPlayer() {
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::D)) {
		mooving = RIGHT;
		playerObj->SetLinearVelocity(b2Vec2(1 * GampeConst::SCALE, playerObj->GetLinearVelocity().y));
	}
	else if (sf::Keyboard::isKeyPressed(sf::Keyboard::A)) {
		mooving = LEFT;
		playerObj->SetLinearVelocity(b2Vec2(-1 * GampeConst::SCALE, playerObj->GetLinearVelocity().y));
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Space)) {
		mooving = JUMP;
		//playerObj->SetLinearVelocity(b2Vec2(playerObj->GetLinearVelocity().x, -40));
		playerObj->ApplyLinearImpulseToCenter(b2Vec2(0, -15), false);
	}
}

b2Body& Player::getPlayerObj() {
	return *playerObj;
}

Player::Mooving& Player::getMoovong() {
	return mooving;
}

Player::Player(sf::String fileTexture, int width, int height) : Entity(fileTexture, width, height) {
	isJump = onGround = false;
	jumpHeight = 0;
	objSprite.setPosition(0, 0);

	/*LeftButton_ = new MoveLeft;
	RightButton_ = new MoveRight;
	JumpButton_ = new MoveJump;*/
}

void Player::setObjectPlayer(b2Body *obj) {
	playerObj = obj;
	objSprite.setPosition(playerObj->GetPosition().x, playerObj->GetPosition().y);
}

Player::~Player(){
	/*delete LeftButton_;
	delete RightButton_;
	delete JumpButton_;*/
}

