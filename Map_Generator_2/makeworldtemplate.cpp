#include "makeworldtemplate.h"

MakeWorldTemplate::MakeWorldTemplate(){
    m_mapWayHeight = 0;
    m_mapWayWidth = 0;

    m_TemplateWidth = 0;
    m_TemplateHeight = 0;

    m_GlobalMap = nullptr;

    m_dirTemplList = "NONE";
}



void MakeWorldTemplate::startMakeGameWorld(MWT_SI **mapWay, MWT_USI width, MWT_USI height){
    templsMap LR, UD, LUR, LDR; // Создание обьектов с шаблонами

    m_mapWayWidth = width;
    m_mapWayHeight = height;

    if (m_dirTemplList == "NONE")
        return;

    pugi::xml_document listTemplates;

    std::string file(m_dirTemplList.c_str());

    if (!listTemplates.load_file(file.c_str()))
        std::cout << "Error load file: " << file << std::endl;

    m_TemplateHeight = listTemplates.child("LoadTemplates").attribute("height").as_uint();
    m_TemplateWidth = listTemplates.child("LoadTemplates").attribute("width").as_uint();

    m_GlobalMapWidth = m_mapWayWidth * m_TemplateWidth;
    m_GlobalMapHeight = m_mapWayHeight * m_TemplateHeight;

    m_GlobalMap = new MWT_SI*[m_GlobalMapHeight];
    for (MWT_USI i = 0; i < m_GlobalMapHeight; i++)
        m_GlobalMap[i] = new MWT_SI[m_GlobalMapWidth];

    for (MWT_USI i = 0; i < m_GlobalMapHeight; i++)
        for (MWT_USI j = 0; j < m_GlobalMapWidth; j++)
            m_GlobalMap[i][j] = 1;

    pugi::xml_node LoadTemplates = listTemplates.child("LoadTemplates");
    pugi::xml_node Load = LoadTemplates.child("Load");

    while (Load){
        if (strcmp(Load.attribute("type").as_string(), "LR") == 0)
            LR.addTemplatesOfFile(Load.attribute("directory").as_string());

        if (strcmp(Load.attribute("type").as_string(), "UD") == 0)
            UD.addTemplatesOfFile(Load.attribute("directory").as_string());

        if (strcmp(Load.attribute("type").as_string(), "LUR") == 0)
            LUR.addTemplatesOfFile(Load.attribute("directory").as_string());

        if (strcmp(Load.attribute("type").as_string(), "LDR") == 0)
            LDR.addTemplatesOfFile(Load.attribute("directory").as_string());

        //std::cout << Load.attribute("type").as_string() << std::endl;

        Load = Load.next_sibling("Load");
    }

    if (LR.m_Data.empty()){
        std::cout << "LR EMPTY";
        return;
    }
    if (UD.m_Data.empty()){
        std::cout << "UD EMPTY";
        return;
    }
    if (LUR.m_Data.empty()){
        std::cout << "LUR EMPTY";
        return;
    }
    if (LDR.m_Data.empty()){
        std::cout << "LDR EMPTY";
        return;
    }

     //* LR - Left, Right
     //* UD Up, Down
     //* LUR Left, Up, Right
     //* LDR Left, Down, Right

    srand(time(0));

    MWT_USI cursorPosX = 0, cursorPosY = 0, posY = m_TemplateHeight, posX = 0;

    for (int i = 0; i < m_mapWayHeight; i++){
        for (int j = 0; j < m_mapWayWidth; j++){
            cursorPosX = posX;
            cursorPosY = posY;

            if (mapWay[i][j] == 1){ // LEFT RIGHT
                MWT_SI **tempTemplate = LR.getRandomTemplate();
                for (int ii = 0; ii < m_TemplateHeight; ii++){
                    for (int jj = 0; jj < m_TemplateWidth; jj++){
                        if (tempTemplate[ii][jj] == 2)
                            tempTemplate[ii][jj] = rand()%2;

                        m_GlobalMap[cursorPosY][cursorPosX] = tempTemplate[ii][jj];

                        ++cursorPosX;
                        if (cursorPosX >= posX+m_TemplateWidth){
                            cursorPosX = posX;
                            cursorPosY++;
                        }
                    }
                }
            }

            if (mapWay[i][j] == 2){ // UP DOWN
                MWT_SI **tempTemplate = UD.getRandomTemplate();
                for (int ii = 0; ii < m_TemplateHeight; ii++){
                    for (int jj = 0; jj < m_TemplateWidth; jj++){
                        if (tempTemplate[ii][jj] == 2)
                            tempTemplate[ii][jj] = rand()%2;
                        m_GlobalMap[cursorPosY][cursorPosX] = tempTemplate[ii][jj];

                        cursorPosX++;
                        if (cursorPosX >= posX+m_TemplateWidth){
                            cursorPosX = posX;
                            cursorPosY++;
                        }
                    }
                }
            }

            if (mapWay[i][j] == 3){ // LEFT UP RIGHT
                MWT_SI **tempTemplate = LUR.getRandomTemplate();
                for (int ii = 0; ii < m_TemplateHeight; ii++){
                    for (int jj = 0; jj < m_TemplateWidth; jj++){
                        if (tempTemplate[ii][jj] == 2)
                            tempTemplate[ii][jj] = rand()%2;
                        m_GlobalMap[cursorPosY][cursorPosX] = tempTemplate[ii][jj];

                        cursorPosX++;
                        if (cursorPosX >= posX+m_TemplateWidth){
                            cursorPosX = posX;
                            cursorPosY++;
                        }
                    }
                }
            }

            if (mapWay[i][j] == 4){ // LEFT DOWN RIGHT
                MWT_SI **tempTemplate = LDR.getRandomTemplate();
                for (int ii = 0; ii < m_TemplateHeight; ii++){
                    for (int jj = 0; jj < m_TemplateWidth; jj++){
                        if (tempTemplate[ii][jj] == 2)
                            tempTemplate[ii][jj] = rand()%2;
                        m_GlobalMap[cursorPosY][cursorPosX] = tempTemplate[ii][jj];

                        cursorPosX++;
                        if (cursorPosX >= posX+m_TemplateWidth){
                            cursorPosX = posX;
                            cursorPosY++;
                        }
                    }
                }
            }

            posX += m_TemplateWidth;
        }

        posX = 0;
        posY += m_TemplateHeight-1;
    }
}

void MakeWorldTemplate::setTemplateFileList(std::string size){
    std::ostringstream file;
    file << "data/world_templates/load_";
    file << size << "_templates.xml";

    m_dirTemplList = file.str();
}

void MakeWorldTemplate::saveMap(std::string fileName){
    pugi::xml_document doc;

    if (!doc.load_file("data/savingSettings/save_map_Template.xml")){
        std::cout << "Error load file: " << "data/savingSettings/save_map_Template.xml\n";
        return;
    }

    pugi::xml_node map = doc.child("map");
    pugi::xml_node template_sizes = map.child("template_sizes");
    pugi::xml_node map_data = map.child("map_data");

    map.attribute("name").set_value("NO_NAME");
    map.attribute("width").set_value(m_GlobalMapWidth);
    map.attribute("height").set_value(m_GlobalMapHeight);

    template_sizes.attribute("width").set_value(m_TemplateWidth);
    template_sizes.attribute("height").set_value(m_TemplateHeight);

    std::ostringstream fileDirectory;
    fileDirectory << "data/world_templates/load_";
    fileDirectory << m_GlobalMapWidth << "x" << m_GlobalMapHeight;
    fileDirectory << "_templates.xml";

    template_sizes.attribute("directory").set_value(fileDirectory.str().c_str());

    std::ostringstream tempMap;

    for (int i = 0; i < m_GlobalMapHeight; i++){
        for (int j = 0; j < m_GlobalMapWidth; j++){
            tempMap << m_GlobalMap[i][j];
        }
        tempMap << "\n";
    }

    map_data.text().set(tempMap.str().c_str());

    std::string directory("data/save_data/maps/");

    if (fileName.empty()){
        doc.save_file(GenFileName::getRandFileName(10, directory, "xml").c_str());
    } else {
        directory += fileName;
        directory += ".xml";
        map.attribute("name").set_value(fileName.c_str());

        doc.save_file(directory.c_str());
    }

}

void MakeWorldTemplate::loadMap(std::string fileName){
    pugi::xml_document doc;

    if (m_GlobalMap != nullptr){
        for (int i = 0; i < m_GlobalMapHeight; i++)
            delete [] m_GlobalMap[i];
        delete [] m_GlobalMap;
    }

    std::string file("data/save_data/maps/");
    file += fileName;
    file += ".xml";

    doc.load_file(file.c_str());

    pugi::xml_node map = doc.child("map");
    pugi::xml_node template_sizes = doc.child("template_sizes");
    pugi::xml_node map_data = map.child("map_data");

    m_GlobalMapWidth = map.attribute("width").as_uint();
    m_GlobalMapHeight = map.attribute("height").as_uint();

    m_TemplateWidth = template_sizes.attribute("width").as_uint();
    m_TemplateHeight = template_sizes.attribute("height").as_uint();
    m_dirTemplList = template_sizes.attribute("directory").as_string();

    std::string TempMapData;
    TempMapData = map_data.text().as_string();

    m_GlobalMap = new MWT_SI*[m_GlobalMapHeight];
    for (MWT_USI i = 0; i < m_GlobalMapHeight; i++)
        m_GlobalMap[i] = new MWT_SI[m_GlobalMapWidth];

    int inStrinng = 0;
    for (MWT_USI i = 0; i < m_GlobalMapHeight; i++){
        for (MWT_USI j = 0; j < m_GlobalMapWidth; j++){
            if (TempMapData[inStrinng] == '0'){
                m_GlobalMap[i][j] = 0;
                ++inStrinng;
            } else {
                m_GlobalMap[i][j] = 1;
                ++inStrinng;
            }
            if (TempMapData[inStrinng] == '\n'){
                inStrinng++;
            }
        }
    }

}

MakeWorldTemplate::MWT_SI** MakeWorldTemplate::getWorld(){
    return m_GlobalMap;
}

MakeWorldTemplate::MWT_USI MakeWorldTemplate::getWorldWidth() const{
    return m_GlobalMapWidth;
}

MakeWorldTemplate::MWT_USI MakeWorldTemplate::getWorldHeight() const{
    return m_GlobalMapHeight;
}

void MakeWorldTemplate::drawWorld() const{
    for (int i = 0; i < m_GlobalMapHeight; i++){
        for (int j = 0; j < m_GlobalMapWidth; j++){
            std::cout.width(3);
            std::cout << m_GlobalMap[i][j];
        }
        std::cout << std::endl;
    }
}

MakeWorldTemplate::~MakeWorldTemplate(){
    for (MWT_USI i = 0; i < m_GlobalMapHeight; i++)
        delete [] m_GlobalMap[i];
    delete [] m_GlobalMap;
}
