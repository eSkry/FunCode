const int nBytesValaue = 8;
int nLedPinsMass[nBytesValaue] = {2,3,4,5,6,7,8,9};
int nBytes[nBytesValaue];

void setup(){
	for (int i = 0; i < nBytesValaue; i++){
		pinMode(nLedPinsMass[i], OUTPUT);
	}
	pinMode(12, INPUT);
}

void loop(){
	int nPinStatus = digitalRead(12);
	int nPinCopy = nPinStatus;

	if (nPinStatus == 1){
		delay(50);
		if (nPinStatus == 1){
			generateValaues();
		}
	}

	if (nPinStatus == 0){
		displayValaues();
	}
}

void generateValaues(){
	for (int s = 0; s < 2; s++){
		for (int i = 0; i < nBytesValaue; i++){
			nBytes[i] = random(0, 2);
			digitalWrite(nLedPinsMass[i], LOW);
		}
	}
}

void displayValaues(){
	for (int i = 0; i < nBytesValaue; i++){
		digitalWrite(nLedPinsMass[i], nBytes[i]);
	}
}