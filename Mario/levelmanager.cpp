#include "levelmanager.h"

LevelManager::LevelManager(System *system){
    lLevel = new Level(system->getRenderWindow());
}

void LevelManager::addMapDirectory(string dir){
    lMapList.push_back( dir );
}

void LevelManager::nextLevel(){
    if (lCurrentLevel != lMapList.end()){
        lCurrentLevel++;
        lLevel->closeLevel();
        lLevel->loadLevel( *lCurrentLevel );
    }
}

void LevelManager::previousLevel(){
    if (lCurrentLevel != lMapList.begin()){
        lCurrentLevel--;
        lLevel->closeLevel();
        lLevel->loadLevel( *lCurrentLevel );
    }
}

int LevelManager::getWorldWidthPixel(){
    return lLevel->getWorldWidthPixel();
}

void LevelManager::selectLevel(int level){
    int i = 0;
    for (auto it = lMapList.begin(); it != lMapList.end(); it++, i++){
        if (i == level){
            lCurrentLevel = it;
            break;
        }
    }
    lLevel->closeLevel();
    lLevel->loadLevel( *lCurrentLevel );
}

void LevelManager::drawLevel(){
    lLevel->drawLevel();
}

Level* LevelManager::getLevel(){
    return lLevel;
}

LevelManager::~LevelManager(){
    delete lLevel;
    lMapList.clear();
}
