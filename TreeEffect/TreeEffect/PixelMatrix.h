#ifndef PIXELMATRIX_H
#define PIXELMATRIX_H

#include <fstream>
#include <string>
#include "Pixel.h"

template <int width_t = 10, int height_t = 10>
class PixelMatrix {
public:
	PixelMatrix();
	~PixelMatrix();

	Pixel matrix[width_t][height_t];

	void saveToFile(std::string fileName);
	//void loadFile(std::string fileName);
};

#endif