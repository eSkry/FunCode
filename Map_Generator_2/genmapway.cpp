#include "genmapway.h"

genMapWay::genMapWay(){
    m_mapWidth = 0;
    m_mapHeight = 0;
    m_genCycles = 0;
    m_map = nullptr;
}

bool genMapWay::startGeneration(GMW_SI width, GMW_SI height, GMW_USI genCycles){
    using std::cout;
    using std::endl;

    if (m_map != nullptr){
        for (int i = 0; i < m_mapHeight; i++)
            delete [] m_map[i];
        delete [] m_map;
    }

    m_mapWidth = width;
    m_mapHeight = height;
    m_genCycles = genCycles;
    m_map = nullptr;

    GMW_SI **tempWorld = new GMW_SI*[height];
    for (GMW_SI i = 0; i < m_mapHeight; i++)
        tempWorld[i] = new GMW_SI[width];

    for (GMW_SI i = 0; i < m_mapHeight; i++)
        for(GMW_SI j = 0; j < m_mapWidth; j++)
            tempWorld[i][j] = 0;

    m_map = tempWorld;

    /*
     * Герерация путей на карте, карта заполняется в некоторых местах значениями в диапазоне от 0 до 12
     * на некотоыре из этих значений стоят ограничения по движению на карте
     *
     * Построение пути происходит рандомно с помощью функции rand(), направления ограничены сторонами
     * если попадается число в диапазоне:
     * 0-5  движение только (LEFT, RIGHT)
     * 6-11  движение только (UP,   DOWN)
     * 12-17  движение только (LEFT, RIGHT, UP)
     * 18-23 движение только (LEFT, RIGHT, DOWN)
     *
     * На карте каждое из перечисленых выше направлений определяется своей цифрой
     *
     * Каждое из представленого диапазона направление вибирается рандомно, например
     * если выпало 2 то мы можем пойти только налево и направо, но выбрать мы можем только одно направление
     * это направление так же выбирается рандомно
     *
     * Строить в самом краю карты запрещено! искольчение составляет верхняя и нижняя строка
     * но в них запрещено строить движение в сторону конца
     */

    srand(time(0)); // Делаем функцию rand() "более рандомной"

    GMW_SI cursorPosX = 1, cursorPosY = 0; // Поциция курсора
    BuilderDirection cursorPosition = NONE; // Напревление движения курсора
    GMW_USI tempCyrsorDirectin = 0; // Для функции rand() для определения направления из представленного диапазона
    GMW_USI valueToMap = 0; // Рандомное значение
    int nums = 0;
    for (GMW_USI cycles = 0; cycles < m_genCycles; cycles++){
        cursorPosX = rand()%(m_mapWidth-4)+1;
        cursorPosY = 0;
        while (cursorPosY != m_mapHeight){
            valueToMap = rand()%4; // Генерация диапазона движений

            ///////////////////////////////////////
            std::cout << nums << " X: " << cursorPosX << " Y: " << cursorPosY << '\n';
            nums++;
            ///////////////////////////////////////

            ///////////// LEFT : RIGHT /////////////
            if (valueToMap == 0){
                while (true){
                    tempCyrsorDirectin = rand()%2;
                    if (tempCyrsorDirectin == 0) // DIR TO LEFT
                        cursorPosition = LEFT;
                    if (tempCyrsorDirectin == 1) // DIR TO RIGHT
                        cursorPosition = RIGHT;

                    if (cursorPosX == 1 && tempCyrsorDirectin == 0) // То нельзя строить в самый край карты повторяем генерацию
                        continue;
                    if (cursorPosX >= m_mapWidth-2 && tempCyrsorDirectin == 1) // То нельзя строить в самый край карты повторяем генерацию
                        continue;

                    // Все в порядке, границы не нарушают
                    break;
                }

                m_map[cursorPosY][cursorPosX] = 1;
                cursorPosition == LEFT ? cursorPosX-- : cursorPosX++;
                continue;
            }
            ///////////// LEFT : RIGHT /////////////

            ///////////// UP : DOWN /////////////
            if (valueToMap == 1){
                while (true){
                    tempCyrsorDirectin = rand()%3;
                    if (tempCyrsorDirectin == 0) // UP
                        cursorPosition = UP;
                    if (tempCyrsorDirectin >= 1) // DOWN
                        cursorPosition = DOWN;

                    if (cursorPosition == UP && cursorPosY <= 1) // То нельзя строить в самый край карты повторяем генерацию
                        continue;
                    if (cursorPosition == DOWN && cursorPosY == m_mapHeight) // То нельзя строить в самый край карты повторяем генерацию
                        continue;

                    // Все в порядке, границы не нарушают
                    break;
                }

                if (cursorPosition == UP){
                    m_map[cursorPosY][cursorPosX] = 5;
                    cursorPosY--;
                }
                if (cursorPosition == DOWN){
                    m_map[cursorPosY][cursorPosX] = 6;
                    cursorPosY++;
                }

                continue;
            }
            ///////////// UP : DOWN /////////////

            ///////////// LEFT : UP : RIGHT /////////////
            if (valueToMap == 2){
                while (true){
                    tempCyrsorDirectin = rand()%3;
                    if (tempCyrsorDirectin == 0) // LEFT
                        cursorPosition = LEFT;
                    if (tempCyrsorDirectin == 1) // UP
                        cursorPosition = UP;
                    if (tempCyrsorDirectin == 2) // RIGHT
                        cursorPosition = RIGHT;

                    if (cursorPosition == UP && cursorPosY == 0)
                        continue;
                    if (cursorPosX == 1 && tempCyrsorDirectin == 0) // То нельзя строить в самый край карты повторяем генерацию
                        continue;
                    if (cursorPosX >= m_mapWidth-2 && tempCyrsorDirectin == 2) // То нельзя строить в самый край карты повторяем генерацию
                        continue;
                    if (cursorPosition == UP && cursorPosY == 0) // То нельзя строить в самый край карты повторяем генерацию
                        continue;

                    // Все в порядке, границы не нарушают
                    break;
                }

                if (cursorPosition == LEFT){
                    m_map[cursorPosY][cursorPosX] = 7;
                    cursorPosX--;
                }
                if (cursorPosition == UP){
                    m_map[cursorPosY][cursorPosX] = 8;
                    cursorPosY--;
                }
                if (cursorPosition == RIGHT){
                    m_map[cursorPosY][cursorPosX] = 9;
                    cursorPosX++;
                }

                continue;
            }
            ///////////// LEFT : UP : RIGHT /////////////

            ///////////// LEFT : DOWN : RIGHT /////////////
            if (valueToMap == 3){
                while (true){
                    tempCyrsorDirectin = rand()%3;
                    if (tempCyrsorDirectin == 0) // LEFT
                        cursorPosition = LEFT;
                    if (tempCyrsorDirectin == 1) //DOWN
                        cursorPosition = DOWN;
                    if (tempCyrsorDirectin == 2) // RIGHT
                        cursorPosition = RIGHT;

                    if (tempCyrsorDirectin == 0 && cursorPosX == 1)
                        continue;
                    if (tempCyrsorDirectin == 1 && cursorPosY == m_mapHeight)
                        continue;
                    if (tempCyrsorDirectin == 2 && cursorPosX == m_mapWidth-2)
                        continue;

                    break;
                }

                if (cursorPosition == LEFT){
                    m_map[cursorPosY][cursorPosX] = 10;
                    cursorPosX--;
                }
                if (cursorPosition == DOWN){
                    m_map[cursorPosY][cursorPosX] = 11;
                    cursorPosY++;
                }
                if (cursorPosition == RIGHT){
                    m_map[cursorPosY][cursorPosX] = 12;
                    cursorPosX++;

                    continue;
                }
            }
            ///////////// LEFT : DOWN : RIGHT /////////////
        }
    }

    return true;
}

void genMapWay::startGenRules(GMW_SI width, GMW_SI height, GMW_USI genCycles){

    if (width == 0 && height == 0)
        return; // 0x0 world?

    if (m_map != nullptr){
        for (int i = 0; i < m_mapHeight; i++)
            delete [] m_map[i];
        delete [] m_map;
        m_map = nullptr;
    }

    m_mapWidth = width;
    m_mapHeight = height;
    m_genCycles = genCycles;

    // Create new map Ways

    GMW_SI **tempWorld = new GMW_SI*[height];
    for (GMW_SI i = 0; i < m_mapHeight; i++)
        tempWorld[i] = new GMW_SI[width];

    for (GMW_SI i = 0; i < m_mapHeight; i++)
        for(GMW_SI j = 0; j < m_mapWidth; j++)
            tempWorld[i][j] = 0;

    m_map = tempWorld;
    ////////////////////////////////////////////////////////
    srand(time(0));


    GMW_SI cursorPosX = 0, cursorPosY = 1; // Позиция места на карте в которое будет устанавливаться значение
    BuilderDirection buildDir = NONE; // Куда будет сделан следующий сдвиг курсора
    GMW_USI dirRand = 0;
    GMW_SI valueInMap = 0; // Переменна для функции rand()
    bool itNormal = false;
    int nums = 0;
    for (GMW_USI cycle = 0; cycle < m_genCycles; cycle++){
        cursorPosX = rand()%(m_mapWidth-4)+1;
        cursorPosY = 1;
        while (cursorPosY < m_mapHeight-1){
            ///////////////////////////////////////
            std::cout << nums << " X: " << cursorPosX << " Y: " << cursorPosY << '\n';
            nums++;
            ///////////////////////////////////////

            valueInMap = rand()%5;
            ///////////// LEFT : RIGHT /////////////
            if (valueInMap <= 1){ // LEFT RIGHT
                while (true) {
                    dirRand = rand()%2;
                    if (dirRand == 1){
                        buildDir = LEFT;
                    } else {
                        buildDir = RIGHT;
                    }

                    if (cursorPosX > m_mapWidth-2){
                        cursorPosX -= m_mapWidth - (m_mapWidth-2);
                        break;
                    }
                    if (m_map[cursorPosY][cursorPosX-1] == 2 || m_map[cursorPosY][cursorPosX+1] == 2) // Если по бокам есть блоки для движения вниз
                        break;
                    if (cursorPosX == 1 && buildDir == LEFT)
                        continue;
                    if (cursorPosX == m_mapWidth && buildDir == RIGHT){
                        cursorPosX--;
                        break;
                    }

                    itNormal = true;
                    break; // Vse ok
                }

                if(itNormal){
                    m_map[cursorPosY][cursorPosX] = 1;
                    buildDir == LEFT ? cursorPosX-- : cursorPosX++;
                    itNormal = false;
                }
                continue;
            }
            ///////////// LEFT : RIGHT /////////////

            ///////////// UP : DOWN /////////////
            if (valueInMap == 2){
                while (true){
                    dirRand = rand()%3;
                    if (dirRand == 0){
                        buildDir = UP;
                    } else {
                        buildDir = DOWN;
                    }

                    if (cursorPosX > m_mapWidth-2)
                        break;
                    if (cursorPosY <= 1)
                        break;
                    if (cursorPosY == m_mapWidth)
                        break;

                    itNormal = true;
                    break;
                }

                if (itNormal){
                    m_map[cursorPosY][cursorPosX] = 2;
                    buildDir == UP ? cursorPosY-- : cursorPosY++;
                    itNormal = false;
                }
                continue;
            }
            ///////////// UP : DOWN /////////////

            ///////////// LEFT : UP : RIGHT /////////////
            if (valueInMap == 3){
                while (true){
                    dirRand = rand()%3;
                    if (dirRand == 0){
                        buildDir = LEFT;
                    } else if (dirRand == 1){
                        buildDir = UP;
                    } else {
                        buildDir = RIGHT;
                    }

                    if (cursorPosX > m_mapWidth-2){
                        cursorPosX -= m_mapWidth - (m_mapWidth-2);
                        break;
                    }
                    if (cursorPosX > m_mapWidth-1 && buildDir == RIGHT)
                        continue;
                    if (cursorPosX == 1)
                        break;
                    if (cursorPosY <= 1 && buildDir == UP)
                        continue;

                    itNormal = true;
                    break;
                }

                if (itNormal){
                    m_map[cursorPosY][cursorPosX] = 3;
                    if (buildDir == LEFT)
                        cursorPosX--;
                    else if (buildDir == UP)
                        cursorPosY--;
                    else cursorPosX++;

                    itNormal = false;
                }

                continue;
            }
            ///////////// LEFT : UP : RIGHT /////////////

            ///////////// LEFT : DOWN : RIGHT /////////////
            if (valueInMap == 4){
                while (true){
                    dirRand = rand()%3;
                    if (dirRand == 0){
                        buildDir = LEFT;
                    } else if (dirRand == 1){
                        buildDir = DOWN;
                    } else {
                        buildDir = RIGHT;
                    }


                    if (cursorPosX > m_mapWidth-2){
                        cursorPosX -= m_mapWidth - (m_mapWidth-2);
                        break;
                    }
                    if (cursorPosX > m_mapWidth-1 && buildDir == RIGHT)
                        continue;
                    if (cursorPosX == 1)
                        break;
                    if (cursorPosY == m_mapHeight && buildDir == DOWN)
                        continue;
                    if (m_map[cursorPosY][cursorPosX-1] == 2 || m_map[cursorPosY][cursorPosX+1] == 2)
                        break;
                    if (m_map[cursorPosY-1][cursorPosX] == 2)
                        break;

                    itNormal = true;
                    break;
                }

                if (itNormal){
                    m_map[cursorPosY][cursorPosX] = 4;
                    if (buildDir == LEFT)
                        cursorPosX--;
                    else if (buildDir == DOWN)
                        cursorPosY++;
                    else cursorPosX++;

                    itNormal = false;
                }
                continue;
            }
            ///////////// LEFT : DOWN : RIGHT /////////////
        }

    }

    testedRandRooms();

    for (int i = 0; i < m_mapHeight; i++){
        for (int j = 0; j < m_mapWidth; j++){
            std::cout << m_map[i][j];
        }
        std::cout << std::endl;
    }

}

void genMapWay::testedRandRooms(){
    srand(time(0));
    int tempCycles = (rand() % (m_mapWidth / 2)) + ((rand()%4)*3);

    GMW_USI x, y;

    for (GMW_USI i = 0; i < tempCycles; i++){
        while (true){
            x = rand() % m_mapWidth;
            y = rand() % m_mapHeight;

            if (x > 1 && x < m_mapWidth-1)
                if (y > 1 && y < m_mapHeight-1)
                    break;
        }

        m_map[y][x] = rand()%5;
    }

}

void genMapWay::loadMapWay(std::string fileName){
    pugi::xml_document doc;

    std::string file("data/save_data/way_maps/");
    file += fileName;
    file += ".xml";

    if (!(doc.load_file(file.c_str()))){
        std::cout << "Error loading file: " << file << '\n';
        return;
    }

    if (m_map != nullptr){
        for (int i = 0; i < m_mapHeight; i++)
            delete [] m_map[i];
        delete [] m_map;
        m_map = nullptr;
    }

    pugi::xml_node mapway = doc.child("mapway");
    pugi::xml_node mapway_data = mapway.child("mapway_data");

    m_mapWidth = atoi(mapway.attribute("width").value());
    m_mapHeight = atoi(mapway.attribute("height").value());
    m_genCycles = mapway.attribute("cycles").as_int();

    m_map = new GMW_SI*[m_mapHeight];
    for (GMW_USI i = 0; i < m_mapHeight; i++)
        m_map[i] = new GMW_SI[m_mapWidth];

    std::string tempStrOne;
    tempStrOne = mapway_data.text().as_string();
    std::string final;

    for (GMW_USI i = 0; i < tempStrOne.length(); i++){
        if (tempStrOne[i] != ',' && tempStrOne[i] != '\n' && tempStrOne[i] != '\t'){
            final += tempStrOne[i];
        }
    }

    GMW_USI ii = 0;
    for (GMW_USI i = 0; i < m_mapHeight; i++)
        for (GMW_USI j = 0; j < m_mapWidth; j++){
            char ch = final[ii];
            m_map[i][j] = atoi(&ch);
            ii++;
        }


}

void genMapWay::saveMapWay(std::string fileName){
    pugi::xml_document doc;
    doc.load_file("data/savingSettings/save_mapWay_template.xml");
    pugi::xml_node mapway = doc.child("mapway");

    if (fileName.empty()){
        mapway.attribute("name").set_value("NO_NAME");
    } else {
        mapway.attribute("name").set_value(fileName.c_str());
    }

    mapway.attribute("width").set_value(m_mapWidth);
    mapway.attribute("height").set_value(m_mapHeight);
    mapway.attribute("cycles").set_value(m_genCycles);

    pugi::xml_node mapway_data = mapway.child("mapway_data");

    std::ostringstream toTypeStr;

    for (int i = 0; i < m_mapHeight; i++){
        for (int j = 0; j < m_mapWidth; j++){
            toTypeStr << m_map[i][j];
        }
        toTypeStr << '\n';
    }

    mapway_data.text().set(toTypeStr.str().c_str());

    std::string directory("data/save_data/way_maps/");

    if (fileName.empty()){
        doc.save_file( GenFileName::getRandFileName(10, directory, "xml").c_str() );
    } else {
        directory += fileName;
        directory += ".xml";

        doc.save_file(directory.c_str());
    }
}

genMapWay::GMW_USI genMapWay::getGenCycles() const{
    return m_genCycles;
}

genMapWay::GMW_SI genMapWay::getMapHeight() const{
    return m_mapHeight;
}

genMapWay::GMW_SI genMapWay::getMapWidth() const{
    return m_mapWidth;
}

genMapWay::GMW_SI** genMapWay::getMap(){
    return m_map;
}

void genMapWay::drawMap() const{
    if (m_map == nullptr){
        std::cout << "Error drawing, Map == nullptr" << std::endl;
        return;
    }

    for (GMW_SI i = 0; i < m_mapHeight; i++){
        for (GMW_SI j = 0; j < m_mapWidth; j++){
            std::cout.width(3);
            std::cout << m_map[i][j];
        }
        std::cout << std::endl;
    }
}

genMapWay::BuilderDirection genMapWay::getRandomDirection() const{
    //srand(time(0));
    GMW_USI temp = rand()%4;

    switch (temp){
    case 0: return UP; break;
    case 1: return DOWN; break;
    case 2: return LEFT; break;
    case 3: return RIGHT; break;
    }

    return NONE;
}

genMapWay::~genMapWay(){
    for (GMW_USI i = 0; i < m_mapHeight; i++)
        delete [] m_map[i];

    delete [] m_map;
}













