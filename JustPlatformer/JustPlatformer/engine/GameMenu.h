#ifndef GAMEMENU_H
#define GAMEMENU_H
#include <SFML/Graphics.hpp>

#include "cursor\Cursor.h"
#include "../betas/lines.h"


class GameMenu {
public:
	GameMenu(sf::RenderWindow &win);
	~GameMenu();

	void update();
	void moveBG(sf::Vector2i &mPos, float &time);
	void drawMenu();
private:
	sf::RenderWindow *pGame_window;
	float time;

	//BackGround
	sf::Texture textureBG;
	sf::Sprite spriteBG;

	//Cursor
	Cursor *cursor;

	//Button Texture

	sf::Texture ETexture;
	sf::Sprite EButton;

	sf::Texture SGTexture;
	sf::Sprite SGButton;

	sf::Texture ITexture;
	sf::Sprite IButton;
};


#endif