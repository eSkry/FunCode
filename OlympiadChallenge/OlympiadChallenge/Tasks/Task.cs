﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OlympiadChallenge.Tasks {
    class Task {

        public Task() {
            ++taskCount;
            currentTaskVal = taskCount;
        }

        public void ShowTask() {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("-------= Задача #{0} =-------", currentTaskVal);
            Console.ResetColor();
        }

        private static UInt32 taskCount = 0;
        private UInt32 currentTaskVal;
    }
}
