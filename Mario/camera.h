#ifndef CAMERA_H
#define CAMERA_H

/////////// MARIO ///////////
#include "system.h"

class Camera{
public:
    Camera(FloatRect rect, IntRect bounds);
    ~Camera();

    void update(Vector2f pos);

    View* getView();
    FloatRect& getRect();


private:
    FloatRect           fRect;
    IntRect             iBounds;
    View*               vView;
};

#endif // CAMERA_H
