
#include "Camera.h"

Camera::Camera(sf::FloatRect windowRect) {
	cameraSpeed = 0.5;

	camera.reset(windowRect);
	camera.setCenter(0, 0);
	camera.zoom(1.5);

}

void Camera::update(int x, int y) {
	camera.setCenter(x, y);
}

void Camera::moovingCamera(float &time) {
	using namespace sf;

	if (Keyboard::isKeyPressed(Keyboard::A)) {
		x += -cameraSpeed * time;
	}
	if (Keyboard::isKeyPressed(Keyboard::D)) {
		x += cameraSpeed * time;
	}
	if (Keyboard::isKeyPressed(Keyboard::W)) {
		y += -cameraSpeed * time;
	}
	if (Keyboard::isKeyPressed(Keyboard::S)) {
		y += cameraSpeed * time;
	}

	if (x < 1200) x = 1200;

	camera.setCenter(x, y);
	
}

sf::View& Camera::getView() {
	return camera;
}

Camera::~Camera() {
}