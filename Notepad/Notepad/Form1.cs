﻿using System;
using System.IO;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Notepad {
    public partial class Notepad : Form {

        string path;

        public Notepad() {
            InitializeComponent();
        }


        private void toolStripButton1_Click_1(object sender, EventArgs e) {
            using(Form2 gg = new Form2()) {
                gg.ShowDialog();
            }
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e) {
            this.Close();
        }

        private void newToolStripMenuItem_Click(object sender, EventArgs e) {
            path = string.Empty;
            textBox1.Clear();
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e) {
            using (OpenFileDialog ofd = new OpenFileDialog() { Filter = "Text Documents|*.txt", ValidateNames = true, Multiselect = false }) {
                if (ofd.ShowDialog() == DialogResult.OK) {
                    using (StreamReader sr = new StreamReader(ofd.FileName)) {
                        path = ofd.FileName;
                        //Task<string> text = sr.ReadToEnd();
                        //textBox1.Text = text.Result;
                        textBox1.Text = sr.ReadToEnd();
                    }
                }
            }
            CheckCountStrings();
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e) {
            if (string.IsNullOrEmpty(path)) {
                using (SaveFileDialog sfd = new SaveFileDialog() { Filter = "Text Document|*.txt", ValidateNames = true, }) {
                    if (sfd.ShowDialog() == DialogResult.OK) {
                        using (StreamWriter sw = new StreamWriter(sfd.FileName)) {
                            sw.Write(textBox1.Text);
                        }
                    }
                }
            } else {
                using (StreamWriter sw = new StreamWriter(path)) {
                    sw.Write(textBox1.Text);
                }
            }
        }

        private void saveAsToolStripMenuItem_Click(object sender, EventArgs e) {
            try {
                using (SaveFileDialog sfd = new SaveFileDialog() { Filter = "Text Document|*.txt", ValidateNames = true, }) {
                    if (sfd.ShowDialog() == DialogResult.OK) {
                        using (StreamWriter sw = new StreamWriter(sfd.FileName)) {
                            sw.Write(textBox1.Text);
                        }
                    }
                }
            } catch (Exception ex) {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void textChanged(object sender, EventArgs e) {
            CheckCountStrings();
        }

        private void CheckCountStrings() {
            UInt32 counter = 0;
            for (int i = 0; i < textBox1.Text.Length; i++) {
                if (textBox1.Text[i] == '\n') ++counter;
            }
            barCountStrings.Text = "Строк: " + counter;
        }
    }
}
