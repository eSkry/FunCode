package eskry.newworld.blocks;

import eskry.newworld.utils.ModInfo;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;

public class ManganeseOreBlock extends Block {

    public ManganeseOreBlock(String name, Material material){
        super(material);
        setBlockName(name);
        setBlockTextureName(ModInfo.modid + ":manganese_ore");
        setCreativeTab(CreativeTabs.tabBlock);
        setHardness(2.0F);
        setResistance(6.0F);
        setLightLevel(0.0F);
        setHarvestLevel("pickaxe", 2);
        setStepSound(soundTypeStone);
    }

}
