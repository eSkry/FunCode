#include <SFML/Graphics.hpp>
#include <string>
/*
#include "genmapway.h"
#include "makeworldtemplate.h"
*/

#include "mapcontroller.h"

int main(){
    sf::RenderWindow window(sf::VideoMode(30*5*10, 12*5*10), "Test");
    window.setFramerateLimit(10);

    MapController mapContrl;
    //mapContrl.setSaveInFile(true);
    //mapContrl.MapWay_setSaveFileName();
    mapContrl.MapWay_setWorldSizes(30, 12);
    mapContrl.MapWay_setCycles(5);
    mapContrl.MapWay_startGeneration();
    //mapContrl.MapWay_saveInFile();
    mapContrl.Map_setTemplateSizes("10x10");
    mapContrl.Map_startGeneration();


    MapController::MC_SI** map = mapContrl.Map_getMap();

    /*
    genMapWay mapp;
    mapp.startGenRules(60, 24, 8);

    //genMapWay::GMW_SI **mappX = mapp.getMap();

    //mapp.startGeneration(32, 16, 8);
    mapp.saveMapWay(nullptr);
    //mapp.loadMapWay("K9SWDYrvOB");
    mapp.drawMap();

    MakeWorldTemplate aaa;
    //aaa.loadMap("00PT3VsxIV");
    aaa.setTemplateFileList("5x5"); // 1 !!!!!
    aaa.startMakeGameWorld(mapp.getMap(), mapp.getMapWidth(), mapp.getMapHeight()); // 2 !!!!!
    aaa.saveMap(nullptr);
    MakeWorldTemplate::MWT_SI **aaaMap = aaa.getWorld();
    //aaa.drawWorld();

    //genMapWay::GMW_SI **map = mapp.getMap();
    genMapWay::GMW_USI mapWidth, mapHeight;
    mapWidth = mapp.getMapWidth();
    mapHeight = mapp.getMapHeight();*/

    sf::Color colors[16];

    for (int i = 0; i < 16; i++)
        colors[i] = sf::Color(rand()%255, rand()%255, rand()%255);

    colors[0] = sf::Color(35, 35, 35);

    sf::Texture texture;
    texture.loadFromFile("data/img/1.png");
    sf::Sprite spr;
    spr.setTexture(texture);
    spr.scale(0.5, 0.5);


    while (window.isOpen()){


        sf::Event event;
        while (window.pollEvent(event)){
            if (event.type == sf::Event::Closed)
                window.close();

            if (event.key.code == sf::Keyboard::Escape)
                window.close();
        }

        window.clear();

        for (int i = 0; i < mapContrl.Map_getHeihgt(); i++){
            for (int j = 0; j < mapContrl.Map_getWidth(); j++){
                spr.setPosition(j*5, i*5);
                spr.setColor(colors[ map[i][j] ]);
                window.draw(spr);
            }
        }

        /*for (int i = 0; i < aaa.getWorldHeight(); i++){
            for (int j = 0; j < aaa.getWorldWidth(); j++){
                spr.setPosition(j*5, i*5);
                spr.setColor(colors[ aaaMap[i][j] ]);
                window.draw(spr);
            }
        }*/

        window.display();
    }

    return 0;
}
