﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SnakeApp {
    class Food {

        public Food() {
            pos = new Vector2i();
            Random rand = new Random();

            // COLOR
            int num = rand.Next() % 3;
            if (num == 0) color = ConsoleColor.DarkMagenta;
            if (num == 1) color = ConsoleColor.Green;
            if (num == 2) color = ConsoleColor.Yellow;

            // SKIN
            num = rand.Next() % 3;
            if (num == 0) skin = '$';
            if (num == 1) skin = '@';
            if (num == 2) skin = 'V';

            // SCORE
            num = rand.Next() % 3;
            if (num == 0) score = 5;
            if (num == 1) score = 10;
            if (num == 2) score = 15;
        }

        public Vector2i pos;
        public char skin;
        public int score;

        public void print() {
            System.Console.SetCursorPosition(pos.x, pos.y);
            Console.ForegroundColor = color;
            System.Console.Write(skin);
            Console.ResetColor();
        }

        private ConsoleColor color;
    }
}
