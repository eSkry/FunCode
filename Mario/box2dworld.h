#ifndef BOX2DWORLD_H
#define BOX2DWORLD_H

/////////// MARIO ///////////
#include "system.h"
#include <Box2D/Box2D.h>

const float SCALE = 30.f;
const float DEG   = 57.29577f;

class Box2DWorld{
public:
    Box2DWorld(b2AABB aabb, b2Vec2 gravity, bool doSleep);
    ~Box2DWorld();

    b2World* getWorld();

    void createGroundBox(FloatRect objRect);
    void createDynamicBox(FloatRect objRect, bool fixedRotation = false);

    b2Body* createPlayer(FloatRect objRect, bool fixedRotation = false);



private:
    b2AABB*                 bWorldAABB; // Ограничивающий бокс мира
    b2World*                bWorld;     // Мир

    b2Vec2                  bGravity;   // Гравитация
    bool                    doSleep;    // true - если обьект попал за границу бокса то он перестает обрабатываться (засыпает);

    b2Body*                 bPlayer;    // Игрок

    list<b2Body*>           lBodysBoxStatic;  // Тела прямоугольники (статичны)
    list<b2Body*>           lBodysBoxDynamic; // ДИнамичные
};

#endif // BOX2DWORLD_H
