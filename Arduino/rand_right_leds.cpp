const int nValPins = 6;
int nLedsPins[nValPins] = {2,3,4,5,6,7};

void setup(){
  for (int i = 0; i < nValPins; i++){
  	pinMode(nLedsPins[i], OUTPUT);
  }
  pinMode(9, INPUT);
}

void loop(){
	int nButton = digitalRead(9);
	if (nButton == 0){
		randomLeds();
	} else {
		rightLeds();
	}
}

void randomLeds(){
	int nRandPos = random(0, nValPins);
	int nRandPos2 = random(0, nValPins);

	if (nRandPos2 == nRandPos){
		for (;nRandPos2 == nRandPos;){
			nRandPos2 = random(0, nValPins);
		}
	}

	digitalWrite(nRandPos, HIGH);
	digitalWrite(nRandPos2, HIGH);
	delay(100);
	digitalWrite(nRandPos2, LOW);
	digitalWrite(nRandPos, LOW);
}

void rightLeds(){
	for (int i = 0; i < nValPins; i++){
		if (i != 0) digitalWrite(nLedsPins[i-1], LOW);
		digitalWrite(nLedsPins[i], HIGH);
		delay(100);
	}
	digitalWrite(nLedsPins[nValPins-1], LOW);
}