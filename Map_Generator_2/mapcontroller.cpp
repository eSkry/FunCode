#include "mapcontroller.h"

MapController::MapController(){
    resetSettings();
}

void MapController::resetSettings(){
    /////////////////////////////
    m_MW_cycles = 1;
    m_MW_height = 10;
    m_MW_width =  10;
    m_MW_isLoadedMap =  false;
    m_MW_saveRandFile = true;
    /////////////////////////////
    /////////////////////////////
    m_MWT_isLoadedMap =  false;
    m_MWT_templateType = "4x4"; // Default
    m_MWT_saveRandFile = true;
    /////////////////////////////
    setSaveInFile(true);
    m_saveInFile = false;
    settings   =   Default;
    /////////////////////////////
}

void MapController::MapWay_setSaveFileName(string fileName){
    if (!fileName.empty()){
        m_MW_SaveFileName = fileName;
        m_MW_isLoadedMap = true;
        settings = Other;
    }
}

void MapController::MapWay_setWorldSizes(MC_USI width, MC_USI height){
    if (width > 2 && height > 2){
        m_MW_width =  width;
        m_MW_height = height;
        settings   =  Other;
    }
}

void MapController::MapWay_loadFromFile(string fileName){
    if (!fileName.empty()){
        m_MW_LoadFileName = fileName;
        settings = Other;
        m_MW_genMapWay.loadMapWay(m_MW_LoadFileName);
    }
}

void MapController::setSaveInFile(bool save){
    m_saveInFile = save;
}

void MapController::MapWay_startGeneration(){
    m_MW_genMapWay.startGenRules(m_MW_width, m_MW_height, m_MW_cycles);
}

genMapWay::GMW_SI** MapController::MapWay_getMapWay(){
    return m_MW_genMapWay.getMap();
}

void MapController::MapWay_setCycles(MC_USI cycles){
    m_MW_cycles = cycles;
}

void MapController::MapWay_saveInFile(){
    if (m_saveInFile){
        if (m_MW_isLoadedMap && m_MW_SaveFileName.empty()){
            m_MW_genMapWay.saveMapWay( m_MW_LoadFileName );
        } else if (!m_MW_SaveFileName.empty()){
            m_MW_genMapWay.saveMapWay( m_MW_SaveFileName );
        } else {
            std::string emptyStr;
            m_MW_genMapWay.saveMapWay(emptyStr);
        }
    }
}

MapController::MC_USI MapController::Map_getWidth() const{
    return m_MWT_makeWorldTemplate.getWorldWidth();
}

MapController::MC_USI MapController::Map_getHeihgt() const{
    return m_MWT_makeWorldTemplate.getWorldHeight();
}

void MapController::Map_setSaveFileName(string fileName){
    m_MWT_SaveFileName = fileName;
}

void MapController::Map_loadFromFile(string fileName){
    m_MWT_LoadFileName = fileName;
    m_MWT_makeWorldTemplate.loadMap(m_MWT_LoadFileName);
}

void MapController::Map_saveInFile(){
    if (m_saveInFile){
        if (m_MWT_SaveFileName.empty()){
            string emptyStr;
            m_MWT_makeWorldTemplate.saveMap(emptyStr);
        } else if (m_MWT_SaveFileName.empty() && !m_MWT_LoadFileName.empty()){
            m_MWT_makeWorldTemplate.saveMap(m_MWT_LoadFileName);
        } else {
            m_MWT_makeWorldTemplate.saveMap(m_MWT_SaveFileName);
        }
    }
}

void MapController::Map_setTemplateSizes(string size){
    m_MWT_templateType = size;
    m_MWT_makeWorldTemplate.setTemplateFileList(m_MWT_templateType);
}

void MapController::Map_startGeneration(){
    m_MWT_makeWorldTemplate.startMakeGameWorld(m_MW_genMapWay.getMap(), m_MW_genMapWay.getMapWidth(), m_MW_genMapWay.getMapHeight());
}

MakeWorldTemplate::MWT_SI** MapController::Map_getMap(){
    return m_MWT_makeWorldTemplate.getWorld();
}




















