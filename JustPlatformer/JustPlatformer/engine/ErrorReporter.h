#ifndef ERROR_REPORTER
#define ERROR_REPORTER

#include "World.h"

template <typename T>
class ErrorReporter {
public:
	char* err(char *str);
private:
	char *errorStr;
};

template <typename T>
inline char* ErrorRporter<T>::err(char *str) {
	return str;
}

#endif // !ERROR_REPORTER
