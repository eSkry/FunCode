#ifndef PLAYER_H
#define PLAYER_H

#include <SFML/Graphics.hpp>
#include "../Entity.h"
#include <Box2D\Box2D.h>

#include "constants.h"
//#include "../InputCommand.h"

class Player : public Entity{
public:

	friend class InputCommand;

	enum Mooving {
		RIGHT,
		LEFT,
		JUMP,
		STAY,
		DOWN,
		UP
	};

	Player();
	Player(sf::String fileTexture, int width, int height);
	~Player();

	void update(float &time);
	void moovingPlayer();

	void setObjectPlayer(b2Body *obj);

	b2Body& getPlayerObj();
	Mooving& getMoovong();
private:
	/*// Keyboard commands
	InputCommand *LeftButton_;
	InputCommand *RightButton_;
	InputCommand *JumpButton_;*/

	b2Body *playerObj;

	Mooving mooving;
	bool onGround, isJump;
	float jumpHeight;
};

#endif