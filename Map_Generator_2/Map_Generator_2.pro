TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

CONFIG(release, debug|release): LIBS += -lsfml-audio -lsfml-graphics -lsfml-network -lsfml-window -lsfml-system
CONFIG(debug, debug|release): LIBS += -lsfml-audio -lsfml-graphics -lsfml-network -lsfml-window -lsfml-system

CONFIG(release, debug|release): LIBS += -lpugixml
CONFIG(debug, debug|release): LIBS += -lpugixml

SOURCES += main.cpp \
    genmapway.cpp \
    makeworldtemplate.cpp \
    templsmap.cpp \
    genfilename.cpp \
    mapcontroller.cpp

HEADERS += \
    genmapway.h \
    makeworldtemplate.h \
    templsmap.h \
    genfilename.h \
    mapcontroller.h
