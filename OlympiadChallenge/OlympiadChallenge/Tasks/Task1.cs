﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

///     Задача #1 (1.1.1 in book)
/// 
/// Даны две целые переменные a, b. Составить фрагмент про-граммы,
/// после исполнения которого значения переменных поменялись
/// бы местами(новое значение a равно старому значению b и наоборот).
/// 

namespace OlympiadChallenge.Tasks {
    class Task1 {

        public static void Run(int a, int b) {
            Task task = new Task();
            task.ShowTask();
            Console.WriteLine("Стартовые значения - a={0}, b={1}", a, b);

            int temp = a;
            a = b;
            b = temp;

            Console.WriteLine("Выходные значения - a={0}, b={1}\n", a, b);
        }

    }
}
