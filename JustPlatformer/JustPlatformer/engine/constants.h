#ifndef CONSTANTS_H
#define CONSTANTS_H

namespace GampeConst {
	const float SCALE = 30.f;
	const float DEG = 57.29577f;
}

#endif