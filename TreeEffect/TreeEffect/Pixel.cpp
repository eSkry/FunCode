#include "Pixel.h"


Pixel::Pixel() {
	r = g = b = a = 255;
}


Pixel::~Pixel() {}

void Pixel::Print() const {
	std::cout << "R: " << r << " G: " << g << " B: " << b << " A: " << a << ' ';
}
