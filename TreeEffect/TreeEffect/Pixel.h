#ifndef PIXEL_H
#define PIXEL_H

#include <iostream>

class Pixel {
public:
	Pixel();
	~Pixel();

	unsigned char r, g, b, a;

	void Print() const;			// ������� � ������� �������� RGBA

};

#endif