import platform
import random

PATH_TO_CONFIG = './'
CONFIG_FILENAME = 'config.csv';

def pathFix(path):			# Дописывает слеш в конец пути если его нет
	length = len(path);
	length = int(length) - 1;
	if (path[length] != '/' and path[length] != '\\'):
		if (platform.system() == "Windows"):
			path += '\\';
		if (platform.system() == "Linux" or platform.system() == "Java"):
			path += '/';
		return path;

	return path;

def fileExists(path, fileName):       # Проверяет существует ли фаил
	try:
		path = pathFix(path);
		file = open(path + fileName, 'r');
	except FileNotFoundError as e:
		print('---=\nWARNING: File \'' + fileName + '\' is not exists!');
		print('In path: ' + path + '\n=---');
		return False;
	else:
		return True;		

def findPosInMassive(splitedLine, name):        # Ищет позицию в массиве по содержимому
        i = 0;
        for line in splitedLine:
                if (name == line):
                        return i;
                i += 1;
                
        print('ERROR: parameter \'' + name + "\' not find in config file (" + CONFIG_FILENAME + ")" );
        return -1;


def getDataFileHeader(path, fileName):	# Возвращает первую строку у файла с данными (Привемр: Date,Time,Open,High,Low,Close,Volume)
	file = open(path + fileName, 'r');
	firstLine = (file.readline()).replace('\n', '');
	firstLine = firstLine.split(',');
	return firstLine;

def stringBuilder(responcedData, fileHeader):
	i = 0;
	data = {1: "str"};
	while i < len(responcedData):
		stringLine = '';
		j = 0;
		lenFileHeader = len(fileHeader);
		while j < lenFileHeader:
			stringLine += str((responcedData[i])[ fileHeader[j] ]);
			if (j < lenFileHeader-1): 
				stringLine += ',';

			j += 1;
		data[i] = stringLine;
		i += 1;
	return data;
		#print(stringLine);

def readLastDate(path, fileName):
	return 'lol'; # нужно реализовать это говно!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


#////////////////////////////////////////////////////////////////////////////////
#Date,Time,Open,High,Low,Close,Volume
#Это имитация возвращаемых значений
def fisabFunc1(_pair, _interval, _type):
	data = [{ 'Date': '28/09/2017', 'Time': str(random.randint(0, 24)) + str(random.randint(0, 60)), 'Open': random.randint(10, 100), 'High': random.randint(10, 100), 'Low': random.randint(10, 100), 'Close': random.randint(10, 100), 'Volume': random.randint(10, 100) },
	{ 'Date': '28/09/2017', 'Time': str(random.randint(0, 24)) + str(random.randint(0, 60)), 'Open': random.randint(10, 100), 'High': random.randint(10, 100), 'Low': random.randint(10, 100), 'Close': random.randint(10, 100), 'Volume': random.randint(10, 100) }];
	return data;

def fisabFunc2(_pair, _interval, _type, _date):
	data = [{ 'Date': '28/09/2017', 'Time': str(random.randint(0, 24)) + str(random.randint(0, 60)), 'Open': random.randint(10, 100), 'High': random.randint(10, 100), 'Low': random.randint(10, 100), 'Close': random.randint(10, 100), 'Volume': random.randint(10, 100) },
	{ 'Date': '28/09/2017', 'Time': str(random.randint(0, 24)) + str(random.randint(0, 60)), 'Open': random.randint(10, 100), 'High': random.randint(10, 100), 'Low': random.randint(10, 100), 'Close': random.randint(10, 100), 'Volume': random.randint(10, 100) }];
	return data;
#////////////////////////////////////////////////////////////////////////////////


def readConfig():       # Читает фаил конфига
	if (fileExists(PATH_TO_CONFIG, CONFIG_FILENAME)):

		firstLine = ''; # Первая строка (будет картой)

		# Позиции элементов (Значения заданы по примеру но это роли не играет).
		pairPos 	= 0;
		intervalPos = 1;
		typePos 	= 2;
		pathPos 	= 3;
		
		confFile = open(CONFIG_FILENAME, 'r');
		currentLine = 0;
		for line in confFile:
			if (currentLine != 0):                  # тут работаем со строками с инфой
				line = line.replace('\n', '');
				data = line.split(',');

				data[pathPos] = data[pathPos].replace('"', ''); #BUG FIX
				data[pathPos] = pathFix(data[pathPos]);

				### Конструирует 'новое' имя файла (Пример: OHLCV_BTC_ETH_5min.csv)
				helpConvert = data[typePos].upper();
				helpConvert = helpConvert.split('_');
				
				newName = '';
				for item in helpConvert:
					newName += item[0];

				newName += '_' + data[pairPos].upper();

				minutes = str(int(int(data[intervalPos]) / 60));
				newName += '_' + minutes + 'min.csv';
				# newName это имя файла созанное на основе инфы в конфиге, пример имени:  OHLCV_BTC_ETH_5min.csv
				### Конструирует 'новое' имя файла (Пример: OHLCV_BTC_ETH_5min.csv)

				#////////////////////////////////////////////////////////////////////////////////
				if (fileExists(data[pathPos], newName)):	#Если такой фаил существует то дополняем его
					lastDate = readLastDate(data[pathPos], newName);
					fileHeader = getDataFileHeader(data[pathPos], newName);
					responcedData = fisabFunc2(data[pairPos], data[intervalPos], data[typePos], lastDate);

					fileForWrite = open(data[pathPos] + newName, 'a');
					gg = stringBuilder(responcedData, fileHeader);
					i = 0;
					while i < len(gg):
						fileForWrite.write('\n' + gg[i]);
						i += 1;
					fileForWrite.close();

				else:
					fileHeader = ['Date', 'Time', 'Open', 'High', 'Low', 'Close', 'Volume'];
					responcedData = fisabFunc1(data[pairPos], data[intervalPos], data[typePos]);
					fileForWrite = open(data[pathPos] + newName, 'a');

					i = 0;
					tempString = '';
					while i < len(fileHeader):
						tempString += fileHeader[i];
						if (i < len(fileHeader) - 1):
							tempString += ',';
						i += 1;
					fileForWrite.write(tempString);

					gg = stringBuilder(responcedData, fileHeader);
					i = 0;
					while i < len(gg):
						fileForWrite.write('\n' + gg[i]);
						i += 1;
					fileForWrite.close();
				#////////////////////////////////////////////////////////////////////////////////

			else:                                   # тут читаем первую строку
				firstLine = line.lower();
				firstLine = firstLine.replace('\n', '');
				firstLine = firstLine.split(',');

				# поиск позиций элементов
				pairPos         = findPosInMassive(firstLine, 'pair');
				intervalPos     = findPosInMassive(firstLine, 'interval');
				typePos         = findPosInMassive(firstLine, 'type');
				pathPos         = findPosInMassive(firstLine, 'path');


			#endif
			
			currentLine += 1;
		#endfor
	#endif




readConfig();


















        
