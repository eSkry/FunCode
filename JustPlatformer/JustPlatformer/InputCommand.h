#ifndef INPUTCOMMAND_H
#define INPUTCOMMAND_H

//#include "Entity.h"
#include "engine\Player.h"

class InputCommand{
public:
	InputCommand();
	~InputCommand();

	virtual void execute(Player &obj) = 0;

};

class MoveRight : public InputCommand {
public:
	explicit MoveRight() {}

	virtual void execute(Entity &obj) {

	}

	virtual void execute(Player &obj) {
		//obj.getMoovong() = Player::RIGHT;
	}
};

class MoveLeft : public InputCommand {
public:
	virtual void execute(Player &obj) {
		//obj.getMoovong() = Player::LEFT;
	}
};

class MoveJump : public InputCommand {
public:
	virtual void execute(Player &obj) {
		//obj.getMoovong() = Player::JUMP;
	}
};

#endif 