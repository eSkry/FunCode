#ifndef GENMAPWAY_H
#define GENMAPWAY_H

#include <iostream>
#include <fstream>
#include <string>
#include <cstring>
#include <sstream>

#include "genfilename.h"
#include "pugixml.hpp"

class genMapWay{
public:
    using GMW_SI = short int;
    using GMW_USI = unsigned short int;

    // TODO: Написать сохранение в фаил

    /*
     * Класс представляет генерацию пути на созданной матрице
     */

    enum BuilderDirection{
        NONE,
        UP,
        DOWN,
        LEFT,
        RIGHT
    };

    genMapWay();
    ~genMapWay();

    void drawMap() const;
    bool startGeneration(GMW_SI width, GMW_SI height, GMW_USI genCycles = 1);
    void startGenRules(GMW_SI width, GMW_SI height, GMW_USI genCycles = 1);

    GMW_USI getGenCycles() const;
    GMW_SI getMapHeight() const;
    GMW_SI getMapWidth() const;
    GMW_SI** getMap();

    void saveMapWay(std::string fileName);
    void loadMapWay(std::string fileName);

private:

    GMW_SI **m_map; // Map
    GMW_SI m_mapWidth, m_mapHeight; // Map Size

    GMW_USI m_genCycles; // Количество проходов Default: 1

    BuilderDirection getRandomDirection() const;

    GMW_SI getDirectionToMap(GMW_USI valueRand, GMW_USI &cursorPosX, GMW_USI &cursorPosY);

    void testedRandRooms();

};

#endif // GENMAPWAY_H
