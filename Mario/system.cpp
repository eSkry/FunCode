#include "system.h"

Configuration::Configuration(){
    fTick               = 40.f;
    sWindowName         = "Default";
    iWindowWidth        = 500;
    iWindowHeight       = 500;
    iFramerateLimit     = 60;
    iGameSpeed          = 800;
    bV_Sinc             = false;
    bCursorVisible      = false;
    uStyle              = sf::Style::Default;
}

System::System(Configuration config){
    cConfiguration          = config;
    fTime                   = 0.f;
    rRenderWindow           = nullptr;
    cColorWindow            = Color(129, 212, 250);
    CreateWindow();
}


void System::mainLoop(Game *game){
    while (rRenderWindow->isOpen()){
    ClockUpdate();
    CursorUpdate();

        while (rRenderWindow->pollEvent(eEvent)){

            if (eEvent.type == Event::Closed)
                rRenderWindow->close();

            game->eventFunc();
        }

        rRenderWindow->clear(cColorWindow);
        game->mainFunc();
        rRenderWindow->display();
    }
}

System::~System(){
    delete rRenderWindow;
}

void System::CreateWindow(){
    VideoMode vVideoMode(cConfiguration.iWindowWidth, cConfiguration.iWindowHeight);

    rRenderWindow = new RenderWindow(vVideoMode, cConfiguration.sWindowName, cConfiguration.uStyle);
    rRenderWindow->setMouseCursorVisible( cConfiguration.bCursorVisible );
    rRenderWindow->setVerticalSyncEnabled( cConfiguration.bV_Sinc );
    rRenderWindow->setFramerateLimit( cConfiguration.iFramerateLimit );
}

void System::ClockUpdate(){
    fTime = cClock.getElapsedTime().asMicroseconds();
    cClock.restart();
    fTime /= cConfiguration.iGameSpeed;

    if (fTime > cConfiguration.fTick) fTime = cConfiguration.fTick;
}

void System::CursorUpdate(){
    vCursorPositionWindow = Mouse::getPosition(*rRenderWindow);
    vCursorPositionGame = rRenderWindow->mapPixelToCoords(vCursorPositionWindow);
}

Mario::Mario(){
    iHealth             = 3;
    iMarioType          = 1;
    iLevel              = 1;
    iScore              = 0;
}

Vector2f& System::getCursorPosGame(){
    return vCursorPositionGame;
}

Vector2i& System::getCursorPosWindow(){
    return vCursorPositionWindow;
}

RenderWindow* System::getRenderWindow(){
    return rRenderWindow;
}

Event& System::getEvent(){
    return eEvent;
}

float System::getTime(){
    return fTime;
}





