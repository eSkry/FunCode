#ifndef CAMERA_H
#define CAMERA_H

#include <SFML/Graphics.hpp>


class Camera {
public:
	explicit Camera(sf::FloatRect windowRect);

	~Camera();

	sf::View& getView();

	void update(int x, int y);
	void moovingCamera(float &time);

private:
	float cameraSpeed;

	sf::View camera;
	float x, y;
};

#endif