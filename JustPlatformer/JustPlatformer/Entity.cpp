#include "Entity.h"

void Entity::setTexture(sf::String fileName) {

}

Entity::Entity(){
	pos.x = pos.y = 0;
	speed = 0;
	objHealth = 100;
	isLife = true;
	width = height = 0;
	dx = 0;
	dy = 0;

	objSprite.setPosition(pos);
}

sf::Vector2f& Entity::getCoords() {
	return pos;
}

sf::Sprite& Entity::getSprite() {
	return objSprite;
}

int Entity::getWidth() const {
	return width;
}

int Entity::getHeight() const {
	return height;
}

void Entity::setPosition(float x, float y) {
	pos.x = x;
	pos.y = y;
}

void Entity::setPosition(sf::Vector2f *pos) {
	this->pos = *pos;
}

Entity::Entity(sf::String fileTexture, int width_, int height_) {
	this->width = width_;
	this->height = height_;

	objTexture.loadFromFile(fileTexture);
	objTexture.setSmooth(true);
	objSprite.setTexture(objTexture);
	objSprite.setOrigin(width / 2, height / 2);
	speed = 0;
	objHealth = 100;
	isLife = true;
	dx = 0;
	dy = 0;
	pos.x = pos.y = 0;
}

Entity::~Entity(){

}
