#ifndef MAKEWORLDTEMPLATE_H
#define MAKEWORLDTEMPLATE_H

#include "pugixml.hpp"

#include <iostream>
#include <sstream>
#include <string>
#include <list>

#include "templsmap.h"
#include "genfilename.h"

class MakeWorldTemplate{
public:
    using MWT_USI = unsigned short int;
    using MWT_SI = short int;

    MakeWorldTemplate();
    ~MakeWorldTemplate();

    MWT_SI** getWorld();
    MWT_USI getWorldWidth() const;
    MWT_USI getWorldHeight() const;

    void startMakeGameWorld(MWT_SI **mapWay, MWT_USI width, MWT_USI height);

    void setTemplateFileList(std::string size);

    void saveMap(std::string fileName);
    void loadMap(std::string fileName);

    void drawWorld() const;
private:
    MWT_USI m_mapWayWidth, m_mapWayHeight;
    MWT_USI m_GlobalMapWidth, m_GlobalMapHeight;
    MWT_USI m_TemplateWidth, m_TemplateHeight;

    MWT_SI **m_GlobalMap;

    std::string m_dirTemplList;

};

#endif // MAKEWORLDTEMPLATE_H
