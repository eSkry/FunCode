﻿using System;
using System.Collections.Generic;
using System.IO;

namespace SnakeApp {
    class Game {

        string map, gameover;
        Player unit;
        int score;
        List<Food> foods;
        const int MAX_FOOD_COUNT = 20;

        public Game() {
            StreamReader fs = new StreamReader("map.txt");
            map = fs.ReadToEnd();
            fs.Close();
            fs = new StreamReader("gameover.txt");
            gameover = fs.ReadToEnd();
            fs.Close();

            unit = new Player(10, 10);
            score = 0;
            foods = new List<Food>();
        }

        public void GameLoop() {
            printMap();
            unit.print();

            while (true) {
                System.Threading.Thread.Sleep(33);
                unit.update();
                newFood();
                collisionCheck();

                printMap();
                unit.print();
            }
        }

        private void collisionCheck() {
            for (int i = 0; i < foods.Count; i++) {
                if (foods[i].pos.equalse(unit.getPosition())) {
                    score += foods[i].score;
                    foods.RemoveAt(i);
                    --i;
                }
            }

            if (map[ (82*unit.getPosition().y) + unit.getPosition().x ] == '#') {// 82 потому, что карта в ширину 80 символов + 1 на символ новой строки и еще +1 из-за левой стены.
                // This is govnokod =)
                Player.Dir dir = unit.getDir();

                switch (dir) {
                    case Player.Dir.UP:     unit.move(0, 1);   break;
                    case Player.Dir.DOWN:   unit.move(0, -1);  break;
                    case Player.Dir.LEFT:   unit.move(1, 0);   break;
                    case Player.Dir.RIGHT:  unit.move(-1, 0);  break;
                }
            }
        }

        private void printMap() {
            Console.SetCursorPosition(0, 0);
            Console.WriteLine(map);
            Console.WriteLine("Score: {0}", score);
            Console.WriteLine("Controls: A W S D");

            for (int i = 0; i < foods.Count; i++) {
                foods[i].print();
            }
        }

        private void newFood() {
            Random rand = new Random();
            while (foods.Count < MAX_FOOD_COUNT) {
                int x = rand.Next() % 80;
                int y = rand.Next() % 20;

                if (map[(82 * y) + x] == ' ') { // 82 потому, что карта в ширину 80 символов + 1 на символ новой строки и еще +1 из-за левой стены.
                    Food temp = new Food();
                    temp.pos.x = x;
                    temp.pos.y = y;
                    foods.Add(temp);
                }


            }
        }
    }
}
