﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OlympiadChallenge {
    class Program {
        static void Main(string[] args) {

            Tasks.Task1.Run(5, 10);
            Tasks.Task2.Run(15, 86);
            Tasks.Task3.Run(6, 7);
        }
    }
}
