#ifndef WORLD_H
#define WORLD_H

#pragma comment(lib, "Box2D.lib")
#pragma comment(lib, "pugixml.lib")

#include <SFML/Graphics.hpp>
#include <Box2D/Box2D.h>
#include "pugixml.hpp"

#include "constants.h"

#include <iostream>
#include <iterator>
#include <vector>
#include <string>
#include <memory>
#include <map>

using std::string;
using std::vector;
using std::map;

class World{
public:
	World();
	~World();

	bool loadMap(char *fileName);

	void errorReporter(char *error) const;

	void drawMap(sf::RenderWindow &game);
	void setTextureTiles();

	b2Body* getPlayerObject();

	enum MAP_STAT {
		LOADED,
		NOT_LOADED
	};

	struct Object {
		string name;
		string type;

		b2Body *body;
	};
	
	struct Tileset {
		int firstgid, tilecount, tileWidth, tileHeight;

		sf::Texture texture;
	};

	struct Layer {
		int width, height;

		vector<int> layer;
	};

	void update();

	// Player object
	b2Body *playerObject = nullptr;

	vector<Object> staticObjects;

	vector<Object> dynamicObjects;
private:
	MAP_STAT loaded;

	int widthMap, heightMap, tileCountInWorld, countTilesetLayers;

	// Box2D params
	b2World *world;
	b2Vec2 *gravitation;


	vector<Tileset> tilesets;
	vector<Layer> layers;
	map<int, sf::Sprite> sprites;

	// ���������� ��� ���������
	int x, y;
};

#endif