#ifndef ENTITY_H
#define ENTITY_H

#include <SFML/Graphics.hpp>

class Entity{
public:
	Entity();
	Entity(sf::String fileTexture, int width_, int height_);
	~Entity();

	void setTexture(sf::String fileName);
	void setPosition(float x, float y);
	void setPosition(sf::Vector2f *pos);

	int getWidth() const;
	int getHeight() const;
	sf::Vector2f& getCoords();
	sf::Sprite& getSprite();
protected:
	sf::Vector2f pos;
	float speed, dx, dy;
	int width, height;
	bool isLife;

	int objHealth;

	sf::Texture objTexture;
	sf::Sprite objSprite;
};

#endif