package eskry.newworld.items;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;

public class SpareParts extends Item {

    public SpareParts(String name, String textureName){
        setUnlocalizedName(name);
        setTextureName("newworld:" + textureName);
        setCreativeTab(CreativeTabs.tabMaterials);
    }

}
