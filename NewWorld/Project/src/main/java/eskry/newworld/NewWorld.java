package eskry.newworld;


import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.registry.GameRegistry;
import eskry.newworld.blocks.RegisterBlocks;
import eskry.newworld.items.ModifedPickaxe;
import eskry.newworld.items.NWItemMaterials;
import eskry.newworld.items.SpareParts;
import eskry.newworld.proxy.CommonProxy;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

@Mod(modid = "newworld")
public class NewWorld {

    public static Item IronPickaxeM1, IronPickaxeM2, IronPickaxeM3, ExtremalPickaxe;
    public static Item SpareParts1, SpareParts2, SpareParts3;

    @SidedProxy(clientSide = "eskry.newworld.proxy.ClientProxy", serverSide = "eskry.newworld.proxy.CommonProxy")
    public static CommonProxy   proxy;

    @Mod.EventHandler
    public void preInit(FMLPreInitializationEvent event){
        proxy.preInit(event);

        // Регистрация итемов.
        IronPickaxeM1 = new ModifedPickaxe("IronPickaxeM1", NWItemMaterials.ToolMatIronPickaxeM1,"iron_pickaxe_m1");
        GameRegistry.registerItem(IronPickaxeM1, "IronPickaxeM1");
        GameRegistry.addShapedRecipe(new ItemStack(NewWorld.IronPickaxeM1)
                ,new Object[]{
                    "IPI", "I I", 'I', Items.iron_ingot, 'P', Items.iron_pickaxe
                });

        IronPickaxeM2 = new ModifedPickaxe("IronPickaxeM2", NWItemMaterials.ToolMatIronPickaxeM2, "iron_pickaxe_m2");
        GameRegistry.registerItem(IronPickaxeM2, "IronPickaxeM2");
        GameRegistry.addShapedRecipe(new ItemStack(NewWorld.IronPickaxeM2)
                ,new Object[]{
                    "P",
                    "I", 'P', NewWorld.IronPickaxeM1, 'I', Items.iron_ingot
                });

        IronPickaxeM3 = new ModifedPickaxe("IronPickaxeM3", NWItemMaterials.ToolMatIronPickaxeM3, "iron_pickaxe_m3");
        GameRegistry.registerItem(IronPickaxeM3, "IronPickaxeM3");

        SpareParts1 = new SpareParts("spareparts1", "pickaxe_spare_parts");
        GameRegistry.registerItem(SpareParts1, "sparetarts1");
        GameRegistry.addRecipe(new ItemStack(NewWorld.SpareParts1)
                , new Object[]{
                    "SRI",
                    "SII", "SII", 'S', Items.stick, 'R', Items.redstone, 'I', Items.iron_ingot
                });

        SpareParts2 = new SpareParts("spareparts2", "pickaxe_spare_parts2");
        GameRegistry.registerItem(SpareParts2, "spareparts1");
        GameRegistry.addRecipe(new ItemStack(NewWorld.SpareParts2)
                ,new Object[]{
                " O ", " S ", "OOO", 'O', Blocks.obsidian, 'S', NewWorld.SpareParts1
                });

        SpareParts3 = new SpareParts("spareparts3", "pickaxe_spare_parts3");
        GameRegistry.addRecipe(new ItemStack(NewWorld.SpareParts3)
                ,new Object[]{
                "DDD", "DSD", "DDD", 'D', Items.diamond, 'S', NewWorld.SpareParts2
                });

        ExtremalPickaxe = new ModifedPickaxe("extremalpickaxe", NWItemMaterials.ToolMaterialExtremalPixkaxe, "extremal_pickaxe");
        GameRegistry.registerItem(ExtremalPickaxe, "extremalpickaxe");
        GameRegistry.addShapedRecipe(new ItemStack(NewWorld.ExtremalPickaxe)
                ,new Object[]{
                "S", "P", 'S', NewWorld.SpareParts3, 'P', Items.diamond_pickaxe
                });

        RegisterBlocks.init();
    }

    @Mod.EventHandler
    public void init(FMLInitializationEvent event){
        proxy.init(event);
    }

    @Mod.EventHandler
    public void postInit(FMLPostInitializationEvent event){
        proxy.postInit(event);
    }

}
