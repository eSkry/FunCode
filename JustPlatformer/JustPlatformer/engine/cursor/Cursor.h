#ifndef CURSOR_H
#define CURSOR_H

#include <SFML/Graphics.hpp>

class Cursor {
public:

	Cursor(sf::String fileName, sf::Vector2i leftTop, sf::Vector2i size);

	void update(sf::Vector2f posMouse);
	sf::Sprite& getSprite();

private:
	sf::Vector2i sizeCursor;

	sf::Texture texture;
	sf::Sprite sprite;

};

#endif
