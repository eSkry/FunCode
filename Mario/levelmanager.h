#ifndef LEVELMANAGER_H
#define LEVELMANAGER_H

/////////// MARIO ///////////
#include "system.h"
#include "level.h"

class LevelManager{
public:
    LevelManager(System *system);
    ~LevelManager();

    void drawLevel();

    // Добавление карты в список
    void addMapDirectory(string dir);

    void nextLevel();

    void previousLevel();

    int getWorldWidthPixel();

    Level* getLevel();

    // Выбор произвольного уровня
    void selectLevel(int level);
private:
    list<string>::iterator  lCurrentLevel;

    list<string>            lMapList; // Список карт

    Level*                  lLevel; // Уровень
};

#endif // LEVELMANAGER_H
