﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

///         Задача №3 (1.1.3)
/// Дано целое число а и натуральное (целое неотрицательное)число n.Вычислить a^n. 
/// Другими словами, необходимо составить про-грамму, при исполнении которой значения переменных а и n не меняют-ся,
/// а значение некоторой другой переменной (например, b) становитсяравным an. 
/// (При этом разрешается использовать и другие переменные.)
/// 

namespace OlympiadChallenge.Tasks {

    class Task3 {

        public static void Run(int a, UInt32 n) {
            Task t = new Task();
            t.ShowTask();
            Console.WriteLine("Стартовые значения - a={0}, b={1}", a, n);
            int result = 1;

            for (int i = 0; i < n; i++) result *= a;

            ShowResult(result);
        }

        private static void ShowResult(int result) {
            Console.WriteLine("Выходные значения - result={0}", result);
        }

    }

}
