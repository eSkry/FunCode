#include "templsmap.h"

templsMap::templsMap(){
    m_Width = 0;
    m_Height = 0;
    m_CountObjects = 0;
}

void templsMap::addTemplatesOfFile(std::string fileName){
    pugi::xml_document doc;

    if (!doc.load_file(fileName.c_str()))
        std::cout << "Error load file " << fileName << std::endl;

    pugi::xml_node size = doc.child("template").child("size");

    m_CountObjects = 0;
    m_Width = size.attribute("width").as_uint();
    m_Height = size.attribute("height").as_uint();

    pugi::xml_node tmpl = doc.child("template").child("tmpl");

    int *tempNumbers = new int[m_Width*m_Height];

    while (tmpl){
        TM_USI iterTiNumbs = 0;
        TM_SI **tempMatrix = new TM_SI *[m_Height];
        for (int i = 0; i < m_Height; i++)
            tempMatrix[i] = new TM_SI[m_Width];

        std::string tempStr = tmpl.text().as_string();

        for (char i : tempStr){
            if (i != ',' && i != '\n' && i != '\t' && i != ' '){
                tempNumbers[iterTiNumbs] = atoi(&i);
                iterTiNumbs++;
            }
        }

        int ii = 0;
        for (int i = 0; i < m_Height; i++){
            for (int j = 0; j < m_Width; j++){
                tempMatrix[i][j] = tempNumbers[ii];
                ii++;
            }
        }

        /*for (int i = 0; i < m_Width*m_Height; i++)
            std::cout << tempNumbers[i];
        std::cout << std::endl;*/

        m_Data.push_back(tempMatrix);
        m_CountObjects++;

        tmpl = tmpl.next_sibling("tmpl");
    }
}

templsMap::TM_SI** templsMap::getRandomTemplate(){
    return m_Data[rand()%m_Data.size()];
}

templsMap::~templsMap(){
    m_Data.clear();
}
