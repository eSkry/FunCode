# FunCode

В данном репозитории нет ничего интересного, здесь просто хранятся мои разнообразные проекты, и весь код пишется по фану.


## Список проектов

Первая программа на C#:
[Unit](https://gitlab.com/eSkry/FunCode/tree/master/Unit)
 (C#)

Енкодер/Декодер Base64:
[Base64 Encoder/Decoder](https://gitlab.com/eSkry/FunCode/tree/master/Base64EncDec)
 (C#, Windows Forms)

Олимпиадные задачи:
[Olympiad Challenge](https://gitlab.com/eSkry/FunCode/tree/master/OlympiadChallenge)
 (C#)

Блокнот:
[Notepad](https://gitlab.com/eSkry/FunCode/tree/master/Notepad)
 (C#, Windows Forms)

Http Сервер:
[Http Server](https://gitlab.com/eSkry/FunCode/tree/master/MyHttpServer)
 (C#, Windows Forms)

Мод для Minecraft:
[NewWorld](https://gitlab.com/eSkry/FunCode/tree/master/NewWorld)
 (Java)

Браузер:
[WebBrowser](https://gitlab.com/eSkry/FunCode/tree/master/WebBrowser)
 (C#, Windows Forms)
 
 @eSkry